import Head from 'next/head'
import { useEffect } from 'react';
import styles from '../styles/Home.module.css'
export default function Home() {
    useEffect(() => {
        const script = document.createElement('script');
        script.defer = true
        script.innerHTML = `
        window.baseUrl = 'https://aissomosiguais.org.br'; window.previousUrl = 'https://aissomosiguais.org.br';
            function modalGallery(modal, capas) {
                modal.showImage = () => {
                    modal.querySelector('.img').style.backgroundImage = \`url('\${modal.images[modal.actualImage].url}')\`
                    modal.querySelector('.texto').innerText = modal.images[modal.actualImage].legend
                }
                modal.nextImage = () => {
                    modal.actualImage++
                    if (modal.actualImage >= modal.images.length) modal.actualImage = 0;
                    modal.showImage()
                }
                modal.prevImage = () => {
                    modal.actualImage--
                    if (modal.actualImage < 0) modal.actualImage = modal.images.length - 1;
                    modal.showImage()
                }
                modal.querySelector('.close').addEventListener('click', () => {
                    $(modal).modal('hide')
                })
                modal.querySelector('.next').addEventListener('click', () => {
                    modal.nextImage()
                })
                modal.querySelector('.prev').addEventListener('click', () => {
                    modal.prevImage()
                })
                capas.forEach(capa => {
                    capa.addEventListener('click', () => {
                        modal.actualImage = 0
                        modal.images = JSON.parse(capa.getAttribute('data-images'))
                        modal.showImage()
                        $(modal).modal('show')
                    })
                })
            }
            modalGallery(document.querySelector('#modalGallery'), document.querySelectorAll('.galeria-fotos .embed-responsive-item'))
            function videos(carousel_videos) {
                let videos = JSON.parse(carousel_videos.getAttribute('videos'))
                let div_template = document.createElement('div')
                div_template.classList.add('embed-responsive')
                div_template.classList.add('embed-responsive-16by9')
                let iframe_template = document.createElement('iframe')
                iframe_template.classList.add('embed-responsive-item')
                let img_template = document.createElement('img')
                img_template.classList.add('embed-responsive-item')
                img_template.style.backgroundRepeat = 'no-repeat'
                img_template.style.backgroundPosition = 'center'
                img_template.style.backgroundSize = 'cover'
                videos.forEach(video => {
                    let div = div_template.cloneNode()
                    video.div = div
                    let iframe = iframe_template.cloneNode()
                    iframe.src = \`\${video.video_embed}?autoplay=1\`
                    video.iframe = iframe
                    let img = img_template.cloneNode()
                    img.style.backgroundImage = \`url('https://img.youtube.com/vi/\${video.video_link}/hqdefault.jpg')\`
                    video.img = img
                    video.showVideo = () => {
                        while (video.div.firstChild)
                            video.div.removeChild(video.div.firstChild)
                        video.div.appendChild(video.iframe)
                    }
                    video.hideVideo = () => {
                        while (video.div.firstChild)
                            video.div.removeChild(video.div.firstChild)
                        video.div.appendChild(video.img)
                    }
                    video.div.addEventListener('click', () => {
                        videos.forEach(video => {
                            video.hideVideo()
                        })
                        video.showVideo()
                    })
                    video.div.appendChild(video.img)
                    carousel_videos.appendChild(div)
                })
            }
            videos(document.querySelector('.videos-container .carousel'))
            function slick(unslick = true) {
                slides = 1
                if (window.innerWidth >
                    768) slides = 2
                if (window.innerWidth >
                    1200) slides = 3
                if (unslick) $('.carousel').slick('unslick')
                $('.carousel').slick({
                    dots: true,
                    infinite: true,
                    speed: 500,
                    slidesToShow: slides,
                    slidesToScroll: slides,
                })
            }
            window.addEventListener('resize', slick)
            slick(false)
            $('.carousel-depoimentos').slick({
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
            })
            $('.materias .slick-slide a').click(function () {
                $('#modalNoticia .img').css('backgroundImage', $(this).find('figure').css('backgroundImage'))
                $('#modalNoticia .titulo').text($(this).find('.titulo').text())
                $('#modalNoticia .texto').html($(this).find('.texto').html())
                $('#modalNoticia').modal('show')
            });
            $('#modalNoticia .close').click(function () {
                $('#modalNoticia').modal('hide')
            })
        `;

        document.querySelector('#__next').appendChild(script);

        return () => {
            document.querySelector('#__next').removeChild(script);
        }
    }, []);
    return (
        <>
        <Head>
            <title>Coral Somos Iguais</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
            <meta http-equiv="X-UA-Compatible" content="ie=edge" />
            <meta name="csrf-token" content="DJihkmsHIjQiGQHlwbW1DZ8ThqXgTMOEydM3vsCy" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="https://aissomosiguais.org.br/images/favicon.ico" type="image/x-icon" />
            <link rel="stylesheet" type="text/css" href="https://aissomosiguais.org.br/css/app.css?id=4d6ee4b5800981d863cc" />
            <link rel="stylesheet" type="text/css" href="https://aissomosiguais.org.br/css/slick.css" />
            <link rel="stylesheet" type="text/css" href="https://aissomosiguais.org.br/css/slick-theme.css?1" />
        </Head>
        <div className="background">
            <header className="header"
                style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/banners/banner_topo_1612184076.png')` }}>
                <div className="padding-nav">
                    <div className="container">
                        <nav className="navbar navbar-expand-lg navbar-light">
                            <img src="https://aissomosiguais.org.br/images/logo-white.png" className="navbar-brand"
                                alt="Coral Somos Iguais" />
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon">
                                </span>
                            </button>
                            <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a className="nav-link" href="#o-projeto">O Projeto</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#videos">Vídeos</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#destaque">Destaque</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#galeria-fotos">Galeria de Fotos</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#contato">Contato</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="banner-text col-12">
                            <h4>
                                PROJETO HUMANITÁRIO</h4>
                            <h1>Coral Somos Iguais</h1>
                            <a href="#destaque">
                                <button className="button-banner">ASSISTIR VÍDEO</button>
                            </a>
                        </div>
                    </div>
                </div>
            </header>
            <section className="home-mission" id="o-projeto">
                <div className="container">
                    <div className="row" style={{ display: 'grid' }}>
                        <h2>O canto de todos os cantos</h2>
                        <h4>MISSÃO</h4>
                        <p>
                            Todos os dias milhares de pessoas s&atilde;o for&ccedil;adas a deixar seus locais de origem,
                            motivadas por diferentes tipos de conflitos. S&oacute; no Brasil, s&atilde;o mais de 11 mil
                            refugiados, pessoas em situa&ccedil;&atilde;o de vulnerabilidade que tentam reconstruir suas
                            vidas, buscando inser&ccedil;&atilde;o, apoio e cidadania. O Coral Somos Iguais est&aacute; se
                            tornando s&iacute;mbolo de apoio &agrave;s causas de seus integrantes, conquistando inclusive o
                            reconhecimento oficial do Alto Comissariado das Na&ccedil;&otilde;es Unidas Para Refugiados
                            (ACNUR). H&aacute; mais de&nbsp;5 anos fazendo da m&uacute;sica o caminho para a
                            integra&ccedil;&atilde;o da comunidade de imigrantes e refugiados &agrave; nossa sociedade,
                            auxiliando na busca por melhores condi&ccedil;&otilde;es de sa&uacute;de, trabalho,
                            educa&ccedil;&atilde;o e moradia.
                    </p>
                    </div>
                </div>
            </section>
            <section className="objetivo container">
                <div className="row pt-5">
                    <div className="col-md-6 d-none d-sm-flex">
                        <img src="https://aissomosiguais.org.br/storage/texthome/IMG_9499_1606240879_1612185980.jpg" />
                    </div>
                    <div className="col-md-6 text">
                        <h2>Dando voz e vez a quem precisa</h2>
                        <h4>OBJETIVO</h4>
                        <p>
                            Abolir fronteiras, integrar culturas, fazer da arte um territ&oacute;rio livre para o
                            desenvolvimento social de quem mais precisa. Esta &eacute; a raz&atilde;o de existir desta
                            iniciativa, um programa humanit&aacute;rio e independente que vem acolhendo in&uacute;meras
                        pessoas e sensibilizando tantas outras por meio da m&uacute;sica.<br />
                            <br />
                        Uma forma de ajudar que se transformou num projeto, onde todos envolvidos voluntariamente
                        cooperam uns com os outros, mostrando o verdadeiro significado da solidariedade, educando a
                        todos sobre o que &eacute; a paz!&nbsp;<br />
                            <br />
                        O Projeto Humanit&aacute;rio-Coral Somos Iguais foi idealizado em 2015 pela volunt&aacute;ria de
                        causas sociais, Daniela Guimar&atilde;es. Dentro do projeto foi criado um coral permanente
                        formado por filhos de imigrantes, de refugiados e por jovens com defici&ecirc;ncias e
                        necessidades especiais, do qual Daniela &eacute; volunt&aacute;ria, e que tem como foco
                        n&atilde;o somente o desenvolvimento musical, mas tamb&eacute;m desenvolver atividades que
                        procura beneficiar seus integrantes com seus objetivos e necessidades, oferecendo suporte
                        psicol&oacute;gico tanto aos integrantes quanto aos seus n&uacute;cleos familiares,&nbsp;que os
                        beneficiem, oferecendo suporte psicol&oacute;gico tanto aos integrantes quanto aos seus
                        n&uacute;cleos familiares.<br />
                            <br />
                        Nosso trabalho procura colaborar com ensino e pesquisa unidos &agrave; f&eacute;, ao amor,
                        &agrave; ci&ecirc;ncia e &agrave; medicina para ajudar a capacitar futuros profissionais em
                        parceria com hospitais nacionais e internacionais de refer&ecirc;ncia, como o HCFMUSP- Hospital
                        das Cl&iacute;nicas da Faculdade de Medicina da Universidade de S&atilde;o Paulo, o maior
                        complexo hospitalar no Brasil e reconhecido mundialmente, fortalecendo nossa assist&ecirc;ncia
                        social.
                    </p>
                    </div>
                </div>
                <div className="row pt-5">
                    <div className="col-md-6 text">
                        <p style={{ textAlign: 'right' }}>
                            Dentre muitos benef&iacute;cios alcan&ccedil;ados perante a conviv&ecirc;ncia de nossos
                            integrantes, destacamos a publica&ccedil;&atilde;o &quot;Improvement of visual acuity after
                            cranioplasty: a new window for functional recovery of post-traumatic visual loss?&quot; feita
                            pelo International Journal Of Medical (Reviews and Case Repost) em 15/04/2019 em Pubmed Style,
                            Web Style, AMA (American Medical Association) Style, Vancouver/ICMJE Style, Havard Style,
                            Turubian Style, Chicago Style, MLA (The Language Association) Style, APA (American Psychological
                        Association) Style.<br />
                            <br />
                        Com esta publica&ccedil;&atilde;o, a equipe de Neurocirurgia do Hospital das Cl&iacute;nicas da
                        Universidade de S&atilde;o Paulo pretende estimular novas investiga&ccedil;&otilde;es neste
                        campo e chamar a aten&ccedil;&atilde;o da comunidade m&eacute;dica para a import&acirc;ncia da
                        cranioplastia como cirurgia funcional, pois pela primeira vez a associa&ccedil;&atilde;o entre a
                        cranioplastia e o retorno da vis&atilde;o de um dos nossos integrantes era incomum, devido ao
                        tempo de evolu&ccedil;&atilde;o da perda visual que este paciente teve.<br />
                        O caso &eacute;&nbsp;citado em congressos de medicina.<br />
                            <br />
                        A.I.S faz um trabalho muito s&eacute;rio e transparente unindo institui&ccedil;&otilde;es e
                        pessoas competentes e experientes para compartilhar informa&ccedil;&otilde;es e conquistar os
                        objetivos de nossos integrantes e assim poder benefici&aacute;-los.
                    </p>
                        <p style={{ textAlign: 'right' }}>
                            Somos Iguais foi selecionada para o Pr&ecirc;mio Innovare na categoria &quot;Justi&ccedil;a e
                            Cidadania. O pr&ecirc;mio tem como objetivo identificar, divulgar e difundir pr&aacute;ticas que
                            contribuam para o aprimoramento da justi&ccedil;a no Brasil.
                    </p>
                        <p style={{ textAlign: 'right' }}>
                            A Comiss&atilde;o Julgadora &eacute; formada por ministros do STF e STJ, desembargadores,
                            promotores, ju&iacute;zes, defensores p&uacute;blicos, advogados e outros profissionais
                            interessados em tornar o Brasil um pa&iacute;s melhor.
                    </p>
                    </div>
                    <div className="col-md-6 d-none d-sm-flex">
                        <img src="https://aissomosiguais.org.br/storage/texthome/IMG_4264_1605032144_1612185989.jpg" />
                    </div>
                </div>
                <div className="row pt-5">
                    <div className="col-md-6 d-none d-sm-flex">
                        <img src="https://aissomosiguais.org.br/storage/texthome/IMG_8123_1603389825_1612185985.jpg" />
                    </div>
                    <div className="col-md-6 text">
                        <h3>Sobre o figurino</h3>
                        <span>
                            <p>
                                Para caracterizar os integrantes do coral em sua primeira apresenta&ccedil;&atilde;o,
                                Daniela convidou o estilista Amir Slama para criar um figurino exclusivo com um conceito
                            diferenciado, sugerindo que os pr&oacute;prios integrantes confeccionassem a arte.<br />
                            Cada um dos integrantes, em seu idioma ou em portugu&ecirc;s, escreveu palavras ou frases
                            que se transformaram na estampa usada em suas roupas.<br />
                            O conceito dos uniformes, principalmente das camisetas, &eacute; fazer com que as roupas
                            usadas sejam tamb&eacute;m uma forma de express&atilde;o e de coloca&ccedil;&atilde;o desses
                            integrantes, para que, durante as suas apresenta&ccedil;&otilde;es, as pessoas pudessem
                            entender e compreender o que elas gostariam de dizer em seus idiomas.<br />
                            O estilista acompanhou os ensaios e conversou com as crian&ccedil;as, que participaram
                            ativamente nas cria&ccedil;&otilde;es de suas roupas, criando juntos a estampa do figurino.
                        </p>
                        </span>
                    </div>
                </div>
                <div className="row pt-5 fala-maestro">
                    <div className="col-md-6 text">
                        <div>
                            <p>
                                A musicoterapia realizada por m&uacute;sicos e terapeutas treinados, ajuda a melhorar os
                            movimentos e habilidades de bra&ccedil;os, pernas e de linguagem dos integrantes.<br />
                            Quando escutamos m&uacute;sica, nosso c&eacute;rebro libera dopamina estimulando mecanismos
                            que causam sensa&ccedil;&atilde;o de motiva&ccedil;&atilde;o e prazer, melhorando a
                            capacidade de se concentrar, aprender e da mem&oacute;ria verbal. A
                            coordena&ccedil;&atilde;o corporal ao ouvir ou tocar algum instrumento, melhora o humor e o
                            psicol&oacute;gico. &nbsp;Com isso os integrantes se beneficiam por se comunicarem entre si
                            de forma l&uacute;dica e socializada para desenvolver suas potencialidades, para recuperar
                            fun&ccedil;&otilde;es de forma que eles possam alcan&ccedil;ar melhor a
                            integra&ccedil;&atilde;o e consequentemente uma melhor qualidade de vida, principalmente os
                            acometidos por uma les&atilde;o cerebral como o AVC.</p>
                            <p style={{ textAlign: 'right' }}>
                                <em>
                                    <strong>
                                        <span style={{ fontFamily: 'Lucida Sans Unicode,Lucida Grande,sans-serif' }}>
                                            Ney Marques </span>
                                    </strong>
                                </em><br />
                            Diretor e Produtor Musical
                        </p>
                        </div>
                    </div>
                    <div className="col-md-6 d-none d-sm-flex">
                        <img
                            src="https://aissomosiguais.org.br/storage/texthome/Frase do maestro_1603910409_1604606933_1612185975.jpg" />
                    </div>
                </div>
            </section>
            <section className="container-fluid materias text-center">
                <div className="container">
                    <div className="row justify-content-center">
                        <h2 className="col-12">Matérias</h2>
                        <div className="col-xl-10 carousel">
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/Screenshot_10_1614000057.jpg')` }}
                                    title="Prêmio Innovare">
                                </figure>
                                <p className="titulo">Prêmio Innovare</p>
                                <div className="texto">
                                    <p>
                                        <a href="https://www.premioinnovare.com.br/praticas/12156"
                                            rel="noopener noreferrer" target="_blank">
                                            https://www.premioinnovare.com.br/praticas/12156
                                    </a>
                                    </p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/Screenshot_9_1613999864.jpg')` }}
                                    title="Refugiado da RDC conquista corações brasileiros no The Voice">
                                </figure>
                                <p className="titulo">
                                    Refugiado da RDC conquista corações brasileiros no The Voice
                                </p>
                                <div className="texto">
                                    <p>
                                        https://www.thepeninsulaqatar.com/article/09/12/2017/DRC-refugee-wins-Brazilian-hearts-in-The-Voice
                                    </p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/Screenshot_8_1613999610.jpg')` }}
                                    title="Grávida Venezuelana Embarca Para o Sul do Brasil">
                                </figure>
                                <p className="titulo">Grávida Venezuelana Embarca Para o Sul do Brasil</p>
                                <div className="texto">
                                    <p>
                                        https://brasil.un.org/pt-br/110815-gravida-venezuelana-embarca-para-o-sul-do-brasil-serei-forte-pelo-meu-filho
                                    </p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/Screenshot_7_1613999513.jpg')` }}
                                    title="UNHCR ACNUR">
                                </figure>
                                <p className="titulo">UNHCR ACNUR</p>
                                <div className="texto">
                                    <p>
                                        <a href="https://www.acnur.org/portugues/2015/12/15/encontro-discute-acesso-de-refugiados-no-mercado-de-trabalho/"
                                            rel="noopener noreferrer" target="_blank">
                                            https://www.acnur.org/portugues/2015/12/15/encontro-discute-acesso-de-refugiados-no-mercado-de-trabalho/
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/materia_onu_no_mackenzie_2015_1612186534.png')` }}
                                    title="Matéria ONU no Mackenzi 2015">
                                </figure>
                                <p className="titulo">Matéria ONU no Mackenzi 2015</p>
                                <div className="texto">
                                    <p>xxxxxxxxxxxxxxxxxxxxx</p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/Frase do maestro_1612185962.jpg')` }}
                                    title="Encontro discute acesso de refugiados no mercado de trabalho">
                                </figure>
                                <p className="titulo">Encontro discute acesso de refugiados no mercado de trabalho</p>
                                <div className="texto">
                                    <p>
                                        Aghiad Shallan, refugiado s&iacute;rio de 42 anos, entra no Audit&oacute;rio da
                                        Universidade Presbiteriana Mackenzie com uma pequena sacola nas
                                        m&atilde;os.<br />
                                        <br />
                                        S&atilde;o Paulo, 15 de dezembro de 2015 (ACNUR) &ndash; Aghiad Shallan,
                                        refugiado s&iacute;rio de 42 anos, entra no Audit&oacute;rio da Universidade
                                        Presbiteriana Mackenzie com uma pequena sacola nas m&atilde;os. Ap&oacute;s
                                        tomar um assento, Aghiad escreveu seu nome e telefone em uma lista que circulava
                                        entre p&uacute;blico presente no encontro &ldquo;Esperan&ccedil;a de um ano novo
                                        aos refugiados&rdquo;, cujo objetivo foi divulgar para diversas empresas uma
                                        lista de refugiados que est&atilde;o &agrave; procura de emprego.
                                    </p>
                                    <p>
                                        &ldquo;Quando eu morava na S&iacute;ria, tinha um bom emprego e uma &oacute;tima
                                        fam&iacute;lia. Minha fam&iacute;lia ainda est&aacute; na S&iacute;ria, quero
                                        traz&ecirc;-los para o Brasil, mas antes preciso de um novo emprego, uma nova
                                        casa&rdquo;, conta.
                                    </p>
                                    <p>
                                        Profissional da &aacute;rea de Tecnologia da Informa&ccedil;&atilde;o (TI),
                                        Aghiad hoje faz apenas bicos como eletricista. Ao dividir a casa em que mora com
                                        outros s&iacute;rios que conheceu no Brasil, Aghiad almeja um emprego fixo que
                                        lhe ofere&ccedil;a mais condi&ccedil;&otilde;es de desenvolvimento. Esta
                                        hist&oacute;ria &eacute; semelhante &agrave; de centenas de outras pessoas que
                                        est&atilde;o em situa&ccedil;&atilde;o de ref&uacute;gio no Brasil.
                                    </p>
                                    <p>
                                        Como forma de facilitar o ingresso de refugiados ao mercado de trabalho no
                                        Brasil, foi realizado na &uacute;ltima sexta-feira (11) um encontro que contou
                                        com a presen&ccedil;a da Ag&ecirc;ncia da ONU para Refugiados (ACNUR) e de
                                        outras representa&ccedil;&otilde;es como o Programa de Apoio para
                                        Recoloca&ccedil;&atilde;o dos Refugiados (PARR), o Sebrae e a Secretaria de
                                        Emprego e Rela&ccedil;&atilde;o do Trabalho &ndash; PAT, entre outros. A ideia
                                        do encontro surgiu ap&oacute;s uma visita de Daniela Guimar&atilde;es,
                                        organizadora do evento, &agrave; Caritas Arquidiocesana de S&atilde;o Paulo,
                                        ocasi&atilde;o em que conheceu a hist&oacute;ria de uma fam&iacute;lia
                                        s&iacute;ria que est&aacute; em situa&ccedil;&atilde;o de ref&uacute;gio no
                                        Brasil.
                                    </p>
                                    <p>
                                        Para Vin&iacute;cius Feitosa, assistente de prote&ccedil;&atilde;o do ACNUR,
                                        &eacute; preciso educar as pessoas e a sociedade em geral sobre o tema do
                                        ref&uacute;gio, que significa uma prote&ccedil;&atilde;o internacional, e
                                        consequentemente o acesso a uma documenta&ccedil;&atilde;o legal para permanecer
                                        em determinado territ&oacute;rio. &ldquo;&Eacute; importante saber que as
                                        pessoas que est&atilde;o chegando v&ecirc;m por motivo for&ccedil;ado, ou seja,
                                        alheio &agrave;s suas vontades. E &eacute; nossa obriga&ccedil;&atilde;o
                                        integr&aacute;-las&rdquo;, disse Vin&iacute;cius. De acordo com ele, h&aacute;
                                        ainda um segundo ponto a ser destacado: a contribui&ccedil;&atilde;o que o
                                        refugiado pode trazer ao mercado de trabalho local. &ldquo;O L&iacute;bano, por
                                        exemplo, recebeu nos &uacute;ltimos dois anos um fluxo de um milh&atilde;o de
                                        refugiados s&iacute;rios. Justamente nesse per&iacute;odo o pa&iacute;s
                                        registrou um crescimento de 2,5 no seu Produto Interno Bruto (PIB)&rdquo;,
                                        disse, ressaltando tamb&eacute;m que outras contribui&ccedil;&otilde;es trazidas
                                        pelos refugiados s&atilde;o percebidas no &acirc;mbito cultural e
                                        lingu&iacute;stico.
                                    </p>
                                    <p>
                                        Uma das vias de integra&ccedil;&atilde;o do refugiado no mercado de trabalho
                                        &eacute; o Programa de Apoio para a Recoloca&ccedil;&atilde;o dos Refugiados
                                        (PARR), parceria entre a consultoria jur&iacute;dica EMDOC, o ACNUR e a
                                        C&aacute;ritas SP &ndash; Centro de Refer&ecirc;ncia para Refugiados.
                                        &ldquo;Atendemos os refugiados na C&aacute;ritas mesmo, para conhecer suas
                                        trajet&oacute;rias profissional e acad&ecirc;mica e em seguida
                                        cadastr&aacute;-los no nosso site. E tamb&eacute;m vamos atr&aacute;s das
                                        empresas, enviamos e-mails, participamos de reuni&otilde;es para divulgar o
                                        tema&rdquo;, explica Mar&iacute;lia Correa, colaboradora do PARR. Os
                                        atendimentos do PARR prestam todas as informa&ccedil;&otilde;es
                                        necess&aacute;rias ao refugiado que busca emprego, como o trajeto &agrave;
                                        empresa e at&eacute; dicas sobre o processo de entrevista.
                                    </p>
                                    <p>
                                        Embora o evento n&atilde;o tenha recebido a presen&ccedil;a aguardada de
                                        representantes do setor privado, Daniela Guimar&atilde;es afirma que novos
                                        encontros com a mesma proposta ser&atilde;o realizados. &ldquo;A gente vai se
                                        fortalecer, continuaremos a organizar essas iniciativas, justamente para
                                        alcan&ccedil;ar o nosso objetivo de os propiciar trabalhos dignos&rdquo;, conta
                                        a idealizadora.
                                    </p>
                                    <p>
                                        Para Aghiad Shallan, o ato de inserir seu nome na lista que ser&aacute; enviada
                                        a representantes o setor empresarial traz esperan&ccedil;a n&atilde;o apenas de
                                        estabilidade financeira, mas de vivenciar um reencontro com quem hoje
                                        est&aacute; distante. &ldquo;Coloquei meu nome naquela lista para encontrar um
                                        emprego, quero trabalhar para poder trazer a minha fam&iacute;lia, que h&aacute;
                                        dois anos n&atilde;o os vejo&rdquo;, diz.
                                    </p>
                                    <p>
                                        <em>Por Nilton Carvalho, de S&atilde;o Paulo.</em>
                                    </p>
                                </div>
                            </div>
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/noticias/refiguados_da_rdc_1612185966.jpg')` }}
                                    title="Refugiado da RDC conquista corações brasileiros em &#039;The Voice&#039;.">
                                </figure>
                                <p className="titulo">Refugiado da RDC conquista corações brasileiros em &#039;The
                                    Voice&#039;.</p>
                                <div className="texto">
                                    <div className="entry-source"
                                        style={{ border: '0px', marginTop: '30px', padding: '0px', textAlign: 'start', textIndent: '0px' }}>
                                        <h4>
                                            <span style={{ fontSize: '14px' }}>
                                                <span style={{ color: '#333333' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <span style={{ fontSize: '18px' }}>
                                                            <span style={{ fontFamily: 'inherit' }}>
                                                                <strong>
                                                                    <span style={{ color: '#000000' }}>AFP</span>
                                                                </strong>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                        </h4>
                                    </div>
                                    <div className="blog-content"
                                        style={{ border: '0px', marginTop: '20px', padding: '0px', textAlign: 'start', textIndent: '0px' }}>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '14px' }}>
                                                <span style={{ color: '#333333' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <span style={{ fontSize: '16px' }}>
                                                            <span style={{ color: '#000000' }}>
                                                                Rio de Janeiro: Ela perdeu seu pa&iacute;s e sua
                                                                inf&acirc;ncia em uma das guerras mais terr&iacute;veis
                                                                da &Aacute;frica, mas a refugiada congolesa Isabel
                                                                Antonio conquistou o cora&ccedil;&atilde;o de
                                                                milh&otilde;es de brasileiros com suas
                                                                apresenta&ccedil;&otilde;es no &quot;The Voice
                                                                Brasil&quot;.
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '14px' }}>
                                                <span style={{ color: '#333333' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <span style={{ fontSize: '16px' }}>
                                                            <span style={{ color: '#000000' }}>
                                                                Com apenas 16 anos, Antonio foi uma das estrelas deste
                                                                ano na vers&atilde;o brasileira do programa de talentos
                                                                da televis&atilde;o.
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                            <br />
                                            <br />
                                            &nbsp;
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        A m&uacute;sica dela?&nbsp;O pac&iacute;fico &quot;Heal the
                                                        World&quot; de Michael Jackson.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Embora ela finalmente tenha sido eliminada na
                                                        ter&ccedil;a-feira, Antonio deixou uma marca no p&uacute;blico -
                                                        e ajudou a trazer esperan&ccedil;a para sua pr&oacute;pria vida
                                                        turbulenta.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &ldquo;Foi muito importante para mim participar deste programa,
                                                        n&atilde;o s&oacute; para mim, mas para outras crian&ccedil;as
                                                        refugiadas. Espero poder ser um exemplo e levar uma mensagem de
                                                        esperan&ccedil;a em meio ao sofrimento&rdquo;, disse ela
                                                        &agrave; AFP .
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Dois anos depois de fugir da Rep&uacute;blica Democr&aacute;tica
                                                        do Congo, Antonio ainda est&aacute; se acostumando com a
                                                        mudan&ccedil;a dram&aacute;tica em sua vida.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Ela mora em S&atilde;o Paulo, mas ap&oacute;s sua &uacute;ltima
                                                        visita aos est&uacute;dios da TV Globo no Rio de Janeiro para o
                                                        concurso desta semana, ela foi ver o mar pela primeira vez.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        O sorriso feliz no rosto ao passear pela praia de Ipanema
                                                        tamb&eacute;m se deveu &agrave; rea&ccedil;&atilde;o dos
                                                        transeuntes.
                                                    </span>
                                                </span>
                                            </span>
                                            <br />
                                            <br />
                                            &nbsp;
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &ldquo;Bravo, voc&ecirc; canta de forma brilhante e &eacute; um
                                                        vencedor aconte&ccedil;a o que acontecer&rdquo;, disse um
                                                        torcedor na rua, Rafael, de 26 anos, que vende gelo.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        A cada poucos passos, ela era interrompida por algu&eacute;m
                                                        pedindo para tirar uma selfie com ela.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <img alt=""
                                                            src="https://www.thepeninsulaqatar.com/kcfinder/upload/images/isabel%20antonio_AFP.jpg"
                                                            style={{ background: '0px 0px', border: '0px', boxSizing: 'border-box', height: 'auto', maxWidth: '100%', outline: '0px', padding: '0px', verticalAlign: 'middle' }} />
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <strong>
                                                            Perdido e se escondendo
                                                        </strong>
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Em 2015, a viol&ecirc;ncia na capital da RDC, Kinshasa,
                                                        for&ccedil;ou sua fam&iacute;lia a fugir.&nbsp;Mas uma tentativa
                                                        de marcar um ponto de encontro caso eles se separassem falhou,
                                                        deixando Antonio e sua irm&atilde; sozinhos e assustados.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &quot;Tive que fugir com minha irm&atilde; mais nova e perdemos
                                                        nossa m&atilde;e. Passamos tr&ecirc;s dias escondidos no
                                                        mato&quot;, lembra ela, tocando nervosamente o longo cabelo
                                                        tran&ccedil;ado enquanto falava.
                                                    </span>
                                                </span>
                                            </span>
                                            <br />
                                            <br />
                                            &nbsp;
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Acabaram sendo resgatados por mission&aacute;rios brasileiros
                                                        que os trouxeram para Angola e de l&aacute; para o Rio de
                                                        Janeiro, mesmo que isso significasse uma nova dor no
                                                        cora&ccedil;&atilde;o.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &quot;N&atilde;o queria ir embora porque queria encontrar minha
                                                        m&atilde;e de novo. Mas aceitei no final porque, do
                                                        contr&aacute;rio, corria o risco de morrer&quot;, disse ela.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        No Brasil, as duas meninas tiveram uma surpresa
                                                        extraordin&aacute;ria quando se inscreveram na
                                                        organiza&ccedil;&atilde;o de caridade cat&oacute;lica Caritas:
                                                        sua m&atilde;e e quatro outros irm&atilde;os haviam feito a
                                                        mesma viagem para S&atilde;o Paulo apenas algumas semanas antes.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        No final de 2015, o pai deles tamb&eacute;m chegou, completando
                                                        o reencontro.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        <strong>
                                                            Voz para quem n&atilde;o tem voz
                                                        </strong>
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Em S&atilde;o Paulo, Antonio descobriu seu talento para o canto
                                                        ao se juntar a um coral chamado Somos Iguais, portugu&ecirc;s
                                                        para We are the Same.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        O coral &eacute; um projeto de Daniela Guimar&atilde;es,
                                                        originalmente apenas para dar &agrave;s crian&ccedil;as algo
                                                        para fazer enquanto suas m&atilde;es eram educadas na
                                                        conscientiza&ccedil;&atilde;o do c&acirc;ncer de mama.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &ldquo;Eles cantaram t&atilde;o bem que senti que devia ir mais
                                                        longe&rdquo;, disse Guimar&atilde;es.
                                                    </span>
                                                </span>
                                            </span>
                                            <br />
                                            <br />
                                            &nbsp;
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Com a ajuda do conhecido diretor de orquestra Jo&atilde;o Carlos
                                                        Martins, o coral - ajudando crian&ccedil;as de Angola, RDC,
                                                        Haiti e S&iacute;ria - come&ccedil;ou a se apresentar em
                                                        v&aacute;rias cidades.&nbsp;Cinco integrantes foram ent&atilde;o
                                                        convidados a fazer um teste para o programa &quot;The Voice
                                                        Kids&quot; da Globo.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        Antonio foi escolhido, mas desde os 16 anos foi convidada a
                                                        voltar e fazer a vers&atilde;o adulta do show.&nbsp;No grande
                                                        palco, ela se saiu bem o suficiente para ir direto para as
                                                        &uacute;ltimas rodadas.
                                                    </span>
                                                </span>
                                            </span>
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            <span style={{ fontSize: '16px' }}>
                                                <span style={{ color: '#000000' }}>
                                                    <span style={{ fontFamily: 'roboto,sans-serif' }}>
                                                        &ldquo;Isabel representa as vozes de todas as outras
                                                        crian&ccedil;as do nosso coral e tem conseguido aproveitar esta
                                                        oportunidade para realizar todos os seus sonhos&rdquo;, disse
                                                        Guimares.
                                                    </span>
                                                </span>
                                            </span>
                                            <br />
                                            <br />
                                            &ldquo;Quando canto, tenho a impress&atilde;o de ser outra pessoa&rdquo;,
                                            disse Antonio durante sua viagem ao Rio.&nbsp;&quot;N&atilde;o sou mais a
                                            pequena Isabel que sofria e tinha medo de morrer.&quot;
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="modal fade" id="modalNoticia">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title titulo">
                            </h5>
                            <button type="button" className="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <figure className="img">
                            </figure>
                            <div className="texto">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section className="depoimentos container">
                <div className="row carousel-depoimentos">
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/WhatsApp Image 2021-02-17 at 12.12.45_1614092429.jpeg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>''2019''</em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Prêmio Innovare<br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/WhatsApp Image 2021-02-17 at 12.12.45 (1)_1614092098.jpeg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Superintendência do Hospital das Clínicas da Faculdade de Medicina da Universidade
                                        de São Paulo.''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                                </p>
                                <h5>
                                    <em>
                                        8° Workshop Brilho nos Olhos
                                    <br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998805.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Você vê o tamanho da dor e da superação que elas tem de passar tudo o que passaram
                                        e chegar aqui sorrindo, cantando...
                                    <br />
                                    Então para mim eu aprendo muito mais com elas do que elas comigo.
                                    <br />
                                    A emoção de viver, a vontade de viver e o respeito pela vida.''
                                </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Ney Marques
                                        <br />
                                        Diretor e produtor musical do Coral Somos Iguais
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998757.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Foi um prazer conhecer todo o trabalho maravilhoso do Coral Somos Iguais. Fiquei
                                        realmente surpresa com a dedicação sincera de Daniela em apoiar esses refugiados e
                                        oferecer-lhes música como fonte de terapia. Não consigo imaginar a orientação e a
                                        segurança que esses refugiados podem sentir ao ter essa oportunidade. Espero
                                        ansiosamente ouvir todo o crescimento do trabalho.''
                                    </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Puneh Ala’i
                                        <br />
                                        Ativista Iraniana
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998714.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''É um dia emocionante para todos nós!
                                    <br />
                                    O Ministério Público hoje pode comemorar uma coisa muito bacana.''
                                </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Gianpaolo Smanio
                                    <br />
                                    Procurador-geral de Justiça do Estado de São Paulo
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998680.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Ainda criança, sonharem com o futuro, não abandone os sonhos nunca que só assim
                                        ele se realizará.''
                                </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Filósofo Clóvis Barros<br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998653.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Coral Somos Iguais, cantaram ao lado do coro formado pelos funcionários do Banco
                                        do Brasil encerrando uma noite que certamente cumpriu seu objetivo de inspirar e
                                        mudar a visão de mundo dos participantes.''
                                </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Inspira BB<br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998541.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''O Coral trouxe a alegria da música e fez as adolescentes refletirem sobre a
                                        condição do refugiado. O recomeço é muito importante para ambos e que gerou
                                        reflexão.''
                                </em>
                                </h4>
                                <p>&nbsp;</p>
                                <h5>
                                    <em>
                                        Ezeilton Santana
                                        <br />
                                        Diretor do CASA Chiquinha Gonzaga, Fundação Casa, Centro de Atendimento
                                        Socioeducativo ao Adolescent
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998513.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Evento como esse nos faz repensar no que tem acontecido em nosso país e no mundo e
                                        nos fazem aumentar a esperança de que 2018 será um ano muito melhor para todos.”''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Desembargador Antônio Carlos Villen
                                    <br />
                                    Diretor da Escola Paulista de Magistratura de São Paulo
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998452.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Participar dos ensaios e da gravação foi uma das coisas mais emocionantes que já
                                        fiz na vida.''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Celso Meireles
                                    <br />
                                    Diretoria de Gestão de Pessoas Banco do Brasil
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998422.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Temos que acreditar que o Brasil tem jeito. Não podemos perder a esperança. Prova
                                        disso são essas crianças refugiadas, que saíram de seus países para tentar
                                        reconstruir uma vida aqui.''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Sidney Beraldo
                                    <br />
                                    Presidente do TCESP Tribunal de Contas do Estado de São Paulo
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998345.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''“O Coral Somos Iguais emocionou e levou parte dos presentes às lágrimas. Mandando
                                        uma mensagem de tolerância, respeito às diversidades culturais e solidariedade.”''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Nucléo de Comunicação Social MInisterio Público do Estado de São Paulo
                                    <br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613997872.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Sabemos que a música transforma a vida das pessoas e vocês estão transformando a
                                        vida dessas crianças''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Desembargador Paulo Dimas Mascaretti
                                    <br />
                                    Presidente Tribunal de Justiça do Estado de São Paulo
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613997795.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Não existe barreira para ajudar!
                                    <br />
                                    O projeto é muito lindo e as crianças do Coral Somos Iguais são incríveis, amorosas
                                    e talentosa.''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Ana Hickmann
                                    <br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613997673.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''Como diz o nome do Projeto Somos Iguais, Se Coloque no Lugar, se nós nos
                                        colocarmos no lugar do outro, nós vamos ver que queremos também uma oportunidade. Eu
                                        acredito que nós não temos só que acolher, nós temos que dar oportunidades. O
                                        Projeto Humanitário foi criado justamente para dar um alívio ao Governo para ajudar
                                        essas pessoas, já que nosso país está em crise e tem obrigações com os cidadãos
                                        brasileiros. Nunca devemos abandonar quem Deus coloca em nosso caminho para ajudar.
                                        É o que eu sempre fiz, faço e vou continuar fazendo. O mundo não precisa de opiniões
                                        e discursos, ele precisa mesmo é de atitudes, independente de quem somos, do que
                                        fazemos, independente dos nossos defeitos ou de nossas qualidades. É o que eu e o
                                        maestro estamos tentando fazer em prol a causa e toda a equipe que está se unindo a
                                        nós."''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Daniela Guimarães
                                    <br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/7245801_0_0_1200_675_1200x0_80_0_1_abc340a4e3b7925bbfdbc6e5836eed26_1613998951.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''''Essa é uma pequena semente plantada no Brasil, Que mostra o significado da
                                        palavra amor.”
                                    <br />
                                    (Padrinho do Projeto Humanitário - Coral Somos Iguais)''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Maestro João Carlos Martins
                                    <br />
                                    </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row justify-content-center">
                            <div className="col-6">
                                <div className="img"
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/depoimento/daniela_guimaraes_1612185971.jpg')` }}>
                                </div>
                            </div>
                            <div className="col-6 py-5">
                                <h4>
                                    <em>
                                        ''É uma honra receber o elogio de ser
                                    <br />
                                    guru de um grande mestre da
                                    <br />
                                    música e da superação!
                                    <br />
                                    Gratidão Maestro.''
                                </em>
                                </h4>
                                <p>
                                    &nbsp;
                            </p>
                                <h5>
                                    <em>
                                        Daniela Guimarães
                                    <br />
                                    Voluntária de Causas Sociais
                                </em>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="bullet-home">
                <div className="row">
                    <div className="col-md-6">
                        <div className="title-bullet">
                            <h2>
                                Inclusção <br />
                                <span>
                                    sem fronteiras
                            </span>
                            </h2>
                        </div>
                    </div>
                    <div className="col-md-6 bullet-padding">
                        <h5>
                            <strong>Na harmonia das vozes, a harmonia entre as diferen&ccedil;as.</strong>
                        </h5>
                        <p>
                            &quot;O nosso projeto, desde o in&iacute;cio, procura se reconhecer perante as
                            diferen&ccedil;as, para enxergarmos a dimens&atilde;o da igualdade entre as pessoas nessa
                            conviv&ecirc;ncia por meio da m&uacute;sica&quot;, diz Daniela Guimar&atilde;es.
                            </p>
                        <p>
                            A aproxima&ccedil;&atilde;o do projeto com a m&uacute;sica, os &oacute;rg&atilde;os
                            judici&aacute;rios, as institui&ccedil;&otilde;es p&uacute;blicas e privadas no Brasil foi um
                            fator consider&aacute;vel na amplia&ccedil;&atilde;o desse trabalho, n&atilde;o somente no
                            sentido de dar visibilidade para a causa de nossos integrantes, mas principalmente pelo
                            sentimento de acolhimento desenvolvido nessas pessoas, da confian&ccedil;a nos
                            &oacute;rg&atilde;os e nas institui&ccedil;&otilde;es. Para eles, estar pr&oacute;ximo de
                            institui&ccedil;&otilde;es p&uacute;blicas prov&ecirc; uma situa&ccedil;&atilde;o de
                            seguran&ccedil;a, esperan&ccedil;a, a cidadania e aos diretos humanos.
                            </p>
                        <h5>
                            <strong>No palco, o lugar onde a inclus&atilde;o n&atilde;o tem fronteiras.</strong>
                        </h5>
                        <p>
                            &Eacute; com essa inspira&ccedil;&atilde;o que o Coral Somos Iguais acumula v&aacute;rias
                            apresenta&ccedil;&otilde;es na bagagem, al&eacute;m de in&uacute;meros admiradores. Entre eles,
                            o pianista e maestro, Jo&atilde;o Carlos Martins, que se sensibilizou pela causa e tornou-se
                            padrinho e colaborador do coral, incluindo-o em algumas de suas apresenta&ccedil;&otilde;es
                            junto &agrave; Orquestra Bachiana Filarm&ocirc;nica.
                        </p>
                        <p>
                            Assim como produtor musical, Ney Marques, que se tornou diretor e produtor musical dos
                            integrantes do coral. &nbsp;
                        </p>
                    </div>
                </div>
            </section>
            <section className="container-fluid materias lohinha text-center">
                <div className="container">
                    <div className="row justify-content-center">
                        <h2 className="col-12">Lojinha</h2>
                        <div className="col-lg-8 carousel">
                            <div>
                                <figure
                                    style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/lojinha/200x300&amp;text=endereco_1612202810.png')` }}
                                    title="Produto 1">
                                </figure>
                                <p className="titulo">
                                    Produto 1
                                </p>
                                <div className="texto">
                                    <p>
                                        Descri&ccedil;&atilde;o do produto 1
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="modal fade" id="modalNoticia">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title titulo">
                            </h5>
                            <button type="button" className="close">
                                <span aria-hidden="true">
                                    &times;
                            </span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <figure className="img">
                            </figure>
                            <div className="texto">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="videos-desktop">
                <section className="videos-home" id="videos">
                    <div className="videos-container">
                        <div className="row">
                            <div className="col-md-12">
                                <h2>
                                    Vídeos
                            </h2>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="videos-container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="carousel"
                                videos="[{&quot;id&quot;:38,&quot;title_video&quot;:&quot;Coral Somos Iguais se apresenta no evento do Facebook no Hotel Royal Palm Plaza em 12\/2019.&quot;,&quot;video_link&quot;:&quot;qzT9V6yn8C8&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:39:51&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:39:51&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/qzT9V6yn8C8&quot;},{&quot;id&quot;:37,&quot;title_video&quot;:&quot;CORAL SOMOS IGUAIS 13.12.2016 We Are Equal&quot;,&quot;video_link&quot;:&quot;VvOewbiL1eY&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:39:20&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:39:20&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/VvOewbiL1eY&quot;},{&quot;id&quot;:36,&quot;title_video&quot;:&quot;Secretaria da Justi\u00e7a e Coral Somos Iguais apoiam campanha. #FiqueEmCasa&quot;,&quot;video_link&quot;:&quot;E0eJpoBm4kA&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:38:55&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:38:55&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/E0eJpoBm4kA&quot;},{&quot;id&quot;:35,&quot;title_video&quot;:&quot;Coral Somos Iguais ensaiando sua musica hino, The Lord Be Magnified para apresenta\u00e7\u00e3o de Natal.&quot;,&quot;video_link&quot;:&quot;uKCG--HSMS4&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:38:05&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:38:05&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/uKCG--HSMS4&quot;},{&quot;id&quot;:34,&quot;title_video&quot;:&quot;Coral Somos Iguais ensaiando a musica Para n\u00e3o ser Triste em 12\/2016!&quot;,&quot;video_link&quot;:&quot;xHVKPqgWyks&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:37:37&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:37:37&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/xHVKPqgWyks&quot;},{&quot;id&quot;:33,&quot;title_video&quot;:&quot;Os integrantes do Coral Somos Iguais colaborando na confec\u00e7\u00e3o de seu figurino em 12\/2016.&quot;,&quot;video_link&quot;:&quot;d8oBsjezzzw&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-12-14 13:36:12&quot;,&quot;updated_at&quot;:&quot;2020-12-14 13:36:12&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/d8oBsjezzzw&quot;},{&quot;id&quot;:32,&quot;title_video&quot;:&quot;Lyus - Hope Right Now&quot;,&quot;video_link&quot;:&quot;ob6tNESiQfg&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-11-24 14:39:44&quot;,&quot;updated_at&quot;:&quot;2020-11-24 14:39:44&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/ob6tNESiQfg&quot;},{&quot;id&quot;:31,&quot;title_video&quot;:&quot;INQU\u00c9RITO apresentando AN\u00d4NIMOS | Making Of Tungst\u00eanio&quot;,&quot;video_link&quot;:&quot;4LqFB0cN7x8&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-11-24 14:39:06&quot;,&quot;updated_at&quot;:&quot;2020-11-24 14:39:06&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/4LqFB0cN7x8&quot;},{&quot;id&quot;:30,&quot;title_video&quot;:&quot;Coral Somos Iguais se apresenta no Tribunal de Contas do Estado de S\u00e3o Paulo na XV Semana Jur\u00eddica&quot;,&quot;video_link&quot;:&quot;7ej1lMCZW_w&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-11-24 14:38:04&quot;,&quot;updated_at&quot;:&quot;2020-11-24 14:38:04&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/7ej1lMCZW_w&quot;},{&quot;id&quot;:29,&quot;title_video&quot;:&quot;Maestro Jo\u00e3o Carlos Martins rege coral formado por refugiados&quot;,&quot;video_link&quot;:&quot;42h-861Fzc0&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:27:41&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:28:51&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/42h-861Fzc0&quot;},{&quot;id&quot;:28,&quot;title_video&quot;:&quot;SOMOS TODOS IGUAIS - LUCAS &amp; LUAN - Feat: Coral Somos Iguais&quot;,&quot;video_link&quot;:&quot;NChd61pAkw8&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:27:10&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:46&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/NChd61pAkw8&quot;},{&quot;id&quot;:27,&quot;title_video&quot;:&quot;V\u00eddeo de final de ano - MPSP&quot;,&quot;video_link&quot;:&quot;Qr-339ofse4&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:26:44&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:48&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/Qr-339ofse4&quot;},{&quot;id&quot;:26,&quot;title_video&quot;:&quot;INQU\u00c9RITO apresentando AN\u00d4NIMOS | Making Of Tungst\u00eanio&quot;,&quot;video_link&quot;:&quot;4LqFB0cN7x8&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:26:11&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:50&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/4LqFB0cN7x8&quot;},{&quot;id&quot;:25,&quot;title_video&quot;:&quot;15\u00ba Sarau no Memorial - Coral MPSP&quot;,&quot;video_link&quot;:&quot;LkQOFTpsT6Y&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:25:45&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:52&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/LkQOFTpsT6Y&quot;},{&quot;id&quot;:24,&quot;title_video&quot;:&quot;Crian\u00e7as do \&quot;CORAL SOMOS IGUAIS \&quot;veem mar pela primeira vez- TVREDE VALE&quot;,&quot;video_link&quot;:&quot;Soj8QEyvfLQ&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:25:24&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:49&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/Soj8QEyvfLQ&quot;},{&quot;id&quot;:23,&quot;title_video&quot;:&quot;Coral Somos Iguais - Exemplo de Uni\u00e3o e Respeito na apresenta\u00e7\u00e3o da Hult Prize&quot;,&quot;video_link&quot;:&quot;VSlhggE0Tek&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:25:09&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:47&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/VSlhggE0Tek&quot;},{&quot;id&quot;:22,&quot;title_video&quot;:&quot;CORAL SOMOS IGUAIS 13.12.2016 We Are Equal&quot;,&quot;video_link&quot;:&quot;VvOewbiL1eY&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:24:45&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:45&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/VvOewbiL1eY&quot;},{&quot;id&quot;:21,&quot;title_video&quot;:&quot;Honda apoia Funda\u00e7\u00e3o Bachiana - Coral Humanit\u00e1rio Somos Iguais&quot;,&quot;video_link&quot;:&quot;fRQObtszJeg&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-04 12:23:17&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:43&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/fRQObtszJeg&quot;},{&quot;id&quot;:20,&quot;title_video&quot;:&quot;Inspira BB - Coral Somos Iguais&quot;,&quot;video_link&quot;:&quot;https:\/\/vimeo.com\/226086509\/84447122d3&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-03-02 14:18:17&quot;,&quot;updated_at&quot;:&quot;2020-03-04 05:51:30&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/https:\/\/vimeo.com\/226086509\/84447122d3&quot;},{&quot;id&quot;:19,&quot;title_video&quot;:&quot;Divers\u00e3o no ensaio do Coral Somos Iguais 40 U2&quot;,&quot;video_link&quot;:&quot;K0F1xy2ioQM&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:31:08&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:39&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/K0F1xy2ioQM&quot;},{&quot;id&quot;:18,&quot;title_video&quot;:&quot;Homenagem para Coral Somos Iguais pelo Dia Mundial dos Refugiados&quot;,&quot;video_link&quot;:&quot;qpIgK8mc8yM&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:30:39&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:37&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/qpIgK8mc8yM&quot;},{&quot;id&quot;:17,&quot;title_video&quot;:&quot;Coral Somos Iguais participa do encerramento A\u00e7\u00e3o do Cora\u00e7\u00e3o - Parte 2&quot;,&quot;video_link&quot;:&quot;n8MPbvCAVxE&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:30:11&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:35&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/n8MPbvCAVxE&quot;},{&quot;id&quot;:16,&quot;title_video&quot;:&quot;Coral Somos Iguais participa do encerramento A\u00e7\u00e3o do Cora\u00e7\u00e3o - Parte 1&quot;,&quot;video_link&quot;:&quot;sAF_pIOUINo&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:29:19&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:18&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/sAF_pIOUINo&quot;},{&quot;id&quot;:15,&quot;title_video&quot;:&quot;Coral Somos Iguais no lan\u00e7amento do selo da A\u00e7\u00e3o do Cora\u00e7\u00e3o&quot;,&quot;video_link&quot;:&quot;jpbwVUd4Rb4&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:28:36&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:16&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/jpbwVUd4Rb4&quot;},{&quot;id&quot;:14,&quot;title_video&quot;:&quot;Coral Somos Iguais e Maestro Jo\u00e3o Carlos Martins - Campanha do Agasalho de 2017 - Jornal Hoje&quot;,&quot;video_link&quot;:&quot;Gb1uB6JU68Q&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:28:18&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:14&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/Gb1uB6JU68Q&quot;},{&quot;id&quot;:13,&quot;title_video&quot;:&quot;Coral Somos Iguais participa da Declara\u00e7\u00e3o Universal de Direitos Humanos&quot;,&quot;video_link&quot;:&quot;5gDiKTICQWs&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:27:12&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:12&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/5gDiKTICQWs&quot;},{&quot;id&quot;:12,&quot;title_video&quot;:&quot;Coral Somos Iguais participa da Declara\u00e7\u00e3o Universal de Direitos Humanos.&quot;,&quot;video_link&quot;:&quot;ttrQo1GpnLY&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:26:41&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:10&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/ttrQo1GpnLY&quot;},{&quot;id&quot;:11,&quot;title_video&quot;:&quot;Coral Somos Iguais participa da leitura da Declara\u00e7\u00e3o Universal de Direitos Humanos.&quot;,&quot;video_link&quot;:&quot;_BxyE9H9dvc&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:26:11&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:08&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/_BxyE9H9dvc&quot;},{&quot;id&quot;:10,&quot;title_video&quot;:&quot;Jo\u00e3o Carlos Martins e Coral Somos Iguais na Campanha do Agasalho 2017&quot;,&quot;video_link&quot;:&quot;ewFiqgJmkFw&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:24:18&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:06&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/ewFiqgJmkFw&quot;},{&quot;id&quot;:8,&quot;title_video&quot;:&quot;Coral Somos Iguais e o Maestro Jo\u00e3o Carlos Martins na noite de NATAL de 2016 - Mat\u00e9ria Fant\u00e1stico&quot;,&quot;video_link&quot;:&quot;OkU3pAumIIA&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-17 06:21:32&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:04&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/OkU3pAumIIA&quot;},{&quot;id&quot;:7,&quot;title_video&quot;:&quot;V\u00eddeo - Teatro Santander SP - Maestro Jo\u00e3o Carlos Martins rege coral formado por refugiados&quot;,&quot;video_link&quot;:&quot;42h-861Fzc0&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-02-03 05:25:55&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:02&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/42h-861Fzc0&quot;},{&quot;id&quot;:5,&quot;title_video&quot;:&quot;Mat\u00e9ria do Coral Somos iguais e Maestro Jo\u00e3o Carlos Martins para o programa Como Ser\u00e1&quot;,&quot;video_link&quot;:&quot;ePSrY0lBRCw&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-01-23 13:09:58&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:31:01&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/ePSrY0lBRCw&quot;},{&quot;id&quot;:4,&quot;title_video&quot;:&quot;Coral Somos Iguais - Apresenta\u00e7\u00e3o de Natal na Esta\u00e7\u00e3o da Luz - Mat\u00e9ria SPTV&quot;,&quot;video_link&quot;:&quot;-N1JQQw8_PY&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-01-23 13:09:43&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:59&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/-N1JQQw8_PY&quot;},{&quot;id&quot;:3,&quot;title_video&quot;:&quot;Coral Somos iguais - Inspira BB Etapa S\u00e3o Paulo&quot;,&quot;video_link&quot;:&quot;IihuEKQauaM&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-01-23 13:09:31&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:57&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/IihuEKQauaM&quot;},{&quot;id&quot;:1,&quot;title_video&quot;:&quot;Coral Somos Iguais e Maestro Jo\u00e3o Carlos Martins na Cerim\u00f4nia de 100 anos da IBM no Brasil&quot;,&quot;video_link&quot;:&quot;kI9ZG4DEfLI&quot;,&quot;active&quot;:1,&quot;created_at&quot;:&quot;2020-01-23 12:20:03&quot;,&quot;updated_at&quot;:&quot;2020-10-28 08:30:55&quot;,&quot;video_embed&quot;:&quot;https:\/\/www.youtube.com\/embed\/kI9ZG4DEfLI&quot;}]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="video-principal" id="destaque">
                <div className="container video-padding">
                    <h2>
                        Do Congo <br />
                    para todo o Brasil
                </h2>
                    <p style={{ textAlign: 'center' }}>
                        Tudo o que &eacute; feito com amor, emo&ccedil;&atilde;o, verdade e talento tende sempre a se
                        propagar. Foi assim que a jovem Isabel Ant&ocirc;nio, oriunda do Congo e integrante do projeto, teve
                        a oportunidade de se tornar a voz s&iacute;mbolo da causa dos refugiados no Brasil. Selecionada para
                        o programa The Voice Brasil, da Rede Globo, esta jovem de 16 anos fez quest&atilde;o de dizer que
                        fazia parte de um coral que defende as causas de seus integrantes por meio da m&uacute;sica,
                        procurando reduzir o preconceito e o choque cultural.
                </p>
                    <p style={{ textAlign: 'center' }}>
                        Isabel Ant&ocirc;nio mostrou seu brilho em rede nacional, conquistando milh&otilde;es de admiradores
                        nacionalmente e internacionalmente, mostrando at&eacute; onde o talento e a
                        determina&ccedil;&atilde;o podem chegar.
                </p>
                    <p style={{ textAlign: 'center' }}>
                    </p>
                </div>
            </div>
            <div className="iframe">
                <iframe width="900" height="500" src="https://www.youtube.com/embed/yBgNrUy_84c&amp;feature=youtu.be"
                    frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen>
                </iframe>
            </div>
            <div className="container galeria-fotos" id="galeria-fotos">
                <h2>
                    Galeria de fotos
            </h2>
                <div className="row carousel">
                    <div className="embed-responsive embed-responsive-16by9">
                        <div className="embed-responsive-item"
                            style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/galerias/1. Dia de gravação.jpg_1614103462.jpg')` }}
                            alt="Álbum 2019"
                            data-images="[{&quot;id&quot;:478,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;1. Dia de grava\u00e7\u00e3o.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de grava\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Dia de grava\u00e7\u00e3o.jpg_1614103462.jpg&quot;},{&quot;id&quot;:479,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o para a CPTM na esta\u00e7\u00e3o da Luz.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o para a CPTM na esta\u00e7\u00e3o da Luz.jpg_1614103462.jpg&quot;},{&quot;id&quot;:480,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;1. Ensaio com Ernesto Teixeira.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Ensaio com Ernesto Teixeira.jpg_1614103462.jpg&quot;},{&quot;id&quot;:481,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;2. Entrega dos presentes de Natal.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Entrega dos presentes de Natal&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Entrega dos presentes de Natal.jpg_1614103462.jpg&quot;},{&quot;id&quot;:482,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;2. Chegando na Arena Corinthias.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Chegando na Arena Corinthias.jpg_1614103462.jpg&quot;},{&quot;id&quot;:483,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;1. A caminho da Arena Corinthias.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. A caminho da Arena Corinthias.jpg_1614103462.jpg&quot;},{&quot;id&quot;:484,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;3. Chegando na Arena Corinthias.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Chegando na Arena Corinthias.jpg_1614103462.jpg&quot;},{&quot;id&quot;:485,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;2. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Comemora\u00e7\u00e3o de Dia das Crian\u00e7as&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103462.jpg&quot;},{&quot;id&quot;:486,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;2. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103462.jpg&quot;},{&quot;id&quot;:487,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;1. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103462.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:04:22&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103462.jpg&quot;},{&quot;id&quot;:488,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;5. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Comemora\u00e7\u00e3o de Dia das Crian\u00e7as&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;},{&quot;id&quot;:489,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o para a CPTM na esta\u00e7\u00e3o da Luz.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o para a CPTM na esta\u00e7\u00e3o da Luz.jpg_1614103533.jpg&quot;},{&quot;id&quot;:490,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;5. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103533.jpg&quot;},{&quot;id&quot;:491,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;5. Carta de agradecimento entre ao Secret\u00e1rio de Justi\u00e7a e Cidadania do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Carta de agradecimento entre ao Secret\u00e1rio de Justi\u00e7a e Cidadania do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;},{&quot;id&quot;:492,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;6. Almo\u00e7o com a ativista iraniana Puneh Ala\u2019I.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Almo\u00e7o com a ativista iraniana Puneh Ala\u2019I&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Almo\u00e7o com a ativista iraniana Puneh Ala\u2019I.jpg_1614103533.jpg&quot;},{&quot;id&quot;:493,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;4. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Comemora\u00e7\u00e3o de Dia das Crian\u00e7as&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;},{&quot;id&quot;:494,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;4. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;},{&quot;id&quot;:495,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;3. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Comemora\u00e7\u00e3o do dia das crian\u00e7as&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Comemora\u00e7\u00e3o do dia das crian\u00e7as.jpg_1614103533.jpg&quot;},{&quot;id&quot;:496,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;4. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103533.jpg&quot;},{&quot;id&quot;:497,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;3. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:05:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Lan\u00e7amento do v\u00eddeo da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103533.jpg&quot;},{&quot;id&quot;:498,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;6. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;6ia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;},{&quot;id&quot;:499,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;7. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;},{&quot;id&quot;:500,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;8. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;},{&quot;id&quot;:501,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;10. Grava\u00e7\u00e3o para o Clipe da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Grava\u00e7\u00e3o para o Clipe da Campanha Imigrante S\u00e3o Paulo te Acolhe do Governo do Estado de S\u00e3o Paulo.jpg_1614103719.jpg&quot;},{&quot;id&quot;:502,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;9. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Dia de apresenta\u00e7\u00e3o e jogo na Arena Corinthias.jpg_1614103719.jpg&quot;},{&quot;id&quot;:503,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;11. Dia de reuni\u00e3o..jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de reuni\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Dia de reuni\u00e3o..jpg_1614103719.jpg&quot;},{&quot;id&quot;:504,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;13. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;},{&quot;id&quot;:505,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;11. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Entrega dos presentes de Natal&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;},{&quot;id&quot;:506,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;12. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;},{&quot;id&quot;:507,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;10. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:08:39&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Apresenta\u00e7\u00e3o nas unidades do sal\u00e3o Studio W de S\u00e3o Paulo.jpg_1614103719.jpg&quot;},{&quot;id&quot;:508,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (5).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (5).jpg_1614103763.jpg&quot;},{&quot;id&quot;:509,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (4).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (4).jpg_1614103763.jpg&quot;},{&quot;id&quot;:510,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (7).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (7).jpg_1614103763.jpg&quot;},{&quot;id&quot;:511,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (8).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (8).jpg_1614103763.jpg&quot;},{&quot;id&quot;:512,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (3).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (3).jpg_1614103763.jpg&quot;},{&quot;id&quot;:513,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (9).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (9).jpg_1614103763.jpg&quot;},{&quot;id&quot;:514,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (6).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (6).jpg_1614103763.jpg&quot;},{&quot;id&quot;:515,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (10).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (10).jpg_1614103763.jpg&quot;},{&quot;id&quot;:516,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (2).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (2).jpg_1614103763.jpg&quot;},{&quot;id&quot;:517,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (1).jpg_1614103763.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:09:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (1).jpg_1614103763.jpg&quot;},{&quot;id&quot;:518,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (13).jpg_1614103907.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:11:47&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (13).jpg_1614103907.jpg&quot;},{&quot;id&quot;:519,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (14).jpg_1614103907.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:11:47&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (14).jpg_1614103907.jpg&quot;},{&quot;id&quot;:520,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (12).jpg_1614103907.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:11:47&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (12).jpg_1614103907.jpg&quot;},{&quot;id&quot;:521,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (11).jpg_1614103907.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:11:47&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (11).jpg_1614103907.jpg&quot;},{&quot;id&quot;:522,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (15).jpg_1614103907.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:11:47&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Grava\u00e7\u00e3o para o Programa Culturando da TV Cultura com Bete Pacheco  (15).jpg_1614103907.jpg&quot;},{&quot;id&quot;:524,&quot;gallery_id&quot;:7,&quot;image&quot;:&quot;image00004.jpeg_1614878280.jpeg&quot;,&quot;created_at&quot;:&quot;2021-03-04 14:18:00&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:15:32&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o Pr\u00eamio Innovare de 2019&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00004.jpeg_1614878280.jpeg&quot;}]">
                        </div>
                        <p>Álbum 2019</p>
                    </div>
                    <div className="embed-responsive embed-responsive-16by9">
                        <div className="embed-responsive-item"
                            style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/galerias/1. Gravação para o vídeo institucional da Honda.jpg_1614103130.jpg')` }}
                            alt="Álbum 2018"
                            data-images="[{&quot;id&quot;:447,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;1. Grava\u00e7\u00e3o para o v\u00eddeo institucional da Honda.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Grava\u00e7\u00e3o para o v\u00eddeo institucional da Honda.jpg_1614103130.jpg&quot;},{&quot;id&quot;:448,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;1. Inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Ensaio Fotogr\u00e1fico integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;},{&quot;id&quot;:449,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o na inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o na inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o na inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;},{&quot;id&quot;:451,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;2. Grava\u00e7\u00e3o para o v\u00eddeo institucional da Honda.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o v\u00eddeo institucional da Honda&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Grava\u00e7\u00e3o para o v\u00eddeo institucional da Honda.jpg_1614103130.jpg&quot;},{&quot;id&quot;:452,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o na inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o na inaugura\u00e7\u00e3o da revitaliza\u00e7\u00e3o do CIC do IMIGRATE da BARRA FUNDA.jpg_1614103130.jpg&quot;},{&quot;id&quot;:453,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;3. Ensaio Fotogr\u00e1fico  integrantes.jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Ensaio Fotogr\u00e1fico  integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Ensaio Fotogr\u00e1fico  integrantes.jpg_1614103130.jpg&quot;},{&quot;id&quot;:454,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;1. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;},{&quot;id&quot;:455,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;},{&quot;id&quot;:456,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:58:50&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103130.jpg&quot;},{&quot;id&quot;:457,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;7. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Ensaio Fotogr\u00e1fico integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;},{&quot;id&quot;:458,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;7. Miguel Pachioni representante do ACNUR (ONU para Refugiados) sempre apoiando nossas iniciativas!.jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Miguel Pachioni representante do ACNUR (ONU para Refugiados) sempre apoiando nossas iniciativas&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Miguel Pachioni representante do ACNUR (ONU para Refugiados) sempre apoiando nossas iniciativas!.jpg_1614103292.jpg&quot;},{&quot;id&quot;:459,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;4. Selo postal em comemora\u00e7\u00e3o \u00e0 7\u00aa A\u00e7\u00e3o do Cora\u00e7\u00e3o \u00e9 lan\u00e7ado no Pal\u00e1cio dos Bandeirantes S\u00e3o Paulo.jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Selo postal em comemora\u00e7\u00e3o \u00e0 7\u00aa A\u00e7\u00e3o do Cora\u00e7\u00e3o \u00e9 lan\u00e7ado no Pal\u00e1cio dos Bandeirantes S\u00e3o Paulo.jpg_1614103292.jpg&quot;},{&quot;id&quot;:460,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;3. Isabel Ant\u00f4nio se preparando para o The Voice Brasil com seus professores de dan\u00e7a..jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Isabel Ant\u00f4nio se preparando para o The Voice Brasil com seus professores de dan\u00e7a..jpg_1614103292.jpg&quot;},{&quot;id&quot;:462,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;4. Dia de ensaio na resid\u00eancia dos integrantes.jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Dia de ensaio na resid\u00eancia dos integrantes.jpg_1614103292.jpg&quot;},{&quot;id&quot;:463,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;5. Apresenta\u00e7\u00e3o na A\u00e7\u00e3o do Cora\u00e7\u00e3o em Santos..jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o na A\u00e7\u00e3o do Cora\u00e7\u00e3o em Santos&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Apresenta\u00e7\u00e3o na A\u00e7\u00e3o do Cora\u00e7\u00e3o em Santos..jpg_1614103292.jpg&quot;},{&quot;id&quot;:464,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;6. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:01:32&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;},{&quot;id&quot;:465,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;5. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103292.jpg&quot;},{&quot;id&quot;:466,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103293.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:01:33&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103293.jpg&quot;},{&quot;id&quot;:467,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;16. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/16. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:468,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;17. Visita de Hungria Hip Hop.jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/17. Visita de Hungria Hip Hop.jpg_1614103354.jpg&quot;},{&quot;id&quot;:470,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;9. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:471,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;13. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:472,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;10. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:473,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;12. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:34&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:474,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;15. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:35&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/15. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103354.jpg&quot;},{&quot;id&quot;:475,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;11. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103355.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:35&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103355.jpg&quot;},{&quot;id&quot;:476,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;14. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103355.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:02:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:02:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Apresenta\u00e7\u00e3o para o comit\u00ea de inser\u00e7\u00e3o ao refugiados do Grupo de Mulheres do Brasil de Luiza Trajano..jpg_1614103355.jpg&quot;},{&quot;id&quot;:477,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;18. Visita de Hungria Hip Hop.jpg_1614103394.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 15:03:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 15:03:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/18. Visita de Hungria Hip Hop.jpg_1614103394.jpg&quot;},{&quot;id&quot;:525,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;image00002.jpeg_1614878315.jpeg&quot;,&quot;created_at&quot;:&quot;2021-03-04 14:18:35&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Ensaio Fotogr\u00e1fico integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00002.jpeg_1614878315.jpeg&quot;},{&quot;id&quot;:526,&quot;gallery_id&quot;:6,&quot;image&quot;:&quot;image00003.jpeg_1614878315.jpeg&quot;,&quot;created_at&quot;:&quot;2021-03-04 14:18:35&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:19:42&quot;,&quot;legend&quot;:&quot;Ensaio Fotogr\u00e1fico integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00003.jpeg_1614878315.jpeg&quot;}]">
                        </div>
                        <p>
                            Álbum 2018
                    </p>
                    </div>
                    <div className="embed-responsive embed-responsive-16by9">
                        <div className="embed-responsive-item"
                            style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/galerias/1. A espera do metrô nosso fiel parceiro para o trajeto das aulas, ensaios e gravações..jpg_1614102003.jpg')` }}
                            alt="Álbum 2017"
                            data-images="[{&quot;id&quot;:297,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. A espera do metr\u00f4 nosso fiel parceiro para o trajeto das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. A espera do metr\u00f4 nosso fiel parceiro para o trajeto das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102003.jpg&quot;},{&quot;id&quot;:298,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102003.jpg&quot;},{&quot;id&quot;:299,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Apresenta\u00e7\u00e3o no F\u00f3rum Criminal da Barra Funda..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Apresenta\u00e7\u00e3o no F\u00f3rum Criminal da Barra Funda..jpg_1614102003.jpg&quot;},{&quot;id&quot;:300,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102003.jpg&quot;},{&quot;id&quot;:301,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102003.jpg&quot;},{&quot;id&quot;:302,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102003.jpg&quot;},{&quot;id&quot;:303,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Ney Marques no bloco de carnaval Friends4ever.jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Ney Marques no bloco de carnaval Friends4ever&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Ney Marques no bloco de carnaval Friends4ever.jpg_1614102003.jpg&quot;},{&quot;id&quot;:304,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Ney Marques sempre atencioso e carinhoso com os integrantes..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Ney Marques sempre atencioso e carinhoso com os integrantes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Ney Marques sempre atencioso e carinhoso com os integrantes..jpg_1614102003.jpg&quot;},{&quot;id&quot;:305,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102003.jpg&quot;},{&quot;id&quot;:306,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102003.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:03&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102003.jpg&quot;},{&quot;id&quot;:307,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. Somos Iguais sempre unidos em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102043.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:43&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:43&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Somos Iguais sempre unidos em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102043.jpg&quot;},{&quot;id&quot;:308,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;1. The Voice Brasil.jpg_1614102043.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;The Voice Brasil&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. The Voice Brasil.jpg_1614102043.jpg&quot;},{&quot;id&quot;:309,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Agni Castro Pita representante Acnurbrasil (ONU para Refugiados) sempre apoiando nossas iniciativas.jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Agni Castro Pita representante Acnurbrasil (ONU para Refugiados) sempre apoiando nossas iniciativas.jpg_1614102044.jpg&quot;},{&quot;id&quot;:310,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Amor e Respeito sempre!.jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Amor e Respeito sempre&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Amor e Respeito sempre!.jpg_1614102044.jpg&quot;},{&quot;id&quot;:311,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o F\u00f3rum Criminal da Barra Funda..jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o F\u00f3rum Criminal da Barra Funda&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o F\u00f3rum Criminal da Barra Funda..jpg_1614102044.jpg&quot;},{&quot;id&quot;:312,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102044.jpg&quot;},{&quot;id&quot;:313,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102044.jpg&quot;},{&quot;id&quot;:314,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102044.jpg&quot;},{&quot;id&quot;:315,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Bloco de carnaval Friends4ever.jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Bloco de carnaval Friends4ever.jpg_1614102044.jpg&quot;},{&quot;id&quot;:316,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102044.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:40:44&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102044.jpg&quot;},{&quot;id&quot;:317,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. Ney Marques preparando nossa integrante Isabel Ant\u00f4nio para o teste do The Voice Brasil..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Ney Marques preparando nossa integrante Isabel Ant\u00f4nio para o teste do The Voice Brasil..jpg_1614102083.jpg&quot;},{&quot;id&quot;:318,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;2. O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. O carinho do Desembargador Villen na apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102083.jpg&quot;},{&quot;id&quot;:319,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Dia de apresenta\u00e7\u00e3o no CitiBank Brasil&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102083.jpg&quot;},{&quot;id&quot;:320,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o no CitiBank Brasil .jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-03-08 14:29:40&quot;,&quot;legend&quot;:&quot;Apresenta\u00e7\u00e3o no CitiBank Brasil&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o no CitiBank Brasil .jpg_1614102083.jpg&quot;},{&quot;id&quot;:321,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102083.jpg&quot;},{&quot;id&quot;:322,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o para Montblanc e UNICEF com Rodrigo Santoro e Ana Hickmann. .jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o para Montblanc e UNICEF com Rodrigo Santoro e Ana Hickmann. .jpg_1614102083.jpg&quot;},{&quot;id&quot;:323,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102083.jpg&quot;},{&quot;id&quot;:324,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Bloco de carnaval Friends4ever.jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Bloco de carnaval Friends4ever.jpg_1614102083.jpg&quot;},{&quot;id&quot;:325,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Dia de ensaio com a presen\u00e7a de uma pessoa vulner\u00e1vel muito querida!.jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Dia de ensaio com a presen\u00e7a de uma pessoa vulner\u00e1vel muito querida!.jpg_1614102083.jpg&quot;},{&quot;id&quot;:326,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Grava\u00e7\u00e3o com a fam\u00edlia de Isabel Ant\u00f4nio para o programa musical The Voice Brasil 2017..jpg_1614102083.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:41:23&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Grava\u00e7\u00e3o com a fam\u00edlia de Isabel Ant\u00f4nio para o programa musical The Voice Brasil 2017..jpg_1614102083.jpg&quot;},{&quot;id&quot;:327,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Grava\u00e7\u00e3o para o Jornal Hoje da rede Globo..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Grava\u00e7\u00e3o para o Jornal Hoje da rede Globo..jpg_1614102130.jpg&quot;},{&quot;id&quot;:328,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102130.jpg&quot;},{&quot;id&quot;:329,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Mensagem deixada no final da apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Mensagem deixada no final da apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;},{&quot;id&quot;:330,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;3. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102130.jpg&quot;},{&quot;id&quot;:331,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o na Assembleia Legislativa do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;},{&quot;id&quot;:332,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102130.jpg&quot;},{&quot;id&quot;:333,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo.jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo.jpg_1614102130.jpg&quot;},{&quot;id&quot;:334,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102130.jpg&quot;},{&quot;id&quot;:335,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Bloco de carnaval Friends4ever.jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Bloco de carnaval Friends4ever.jpg_1614102130.jpg&quot;},{&quot;id&quot;:336,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Dia de ensaio na companhia dessa querida moradora de rua!.jpg_1614102130.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:42:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Dia de ensaio na companhia dessa querida moradora de rua!.jpg_1614102130.jpg&quot;},{&quot;id&quot;:337,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102215.jpg&quot;},{&quot;id&quot;:338,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Mensagem deixada no final da apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Mensagem deixada no final da apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614102215.jpg&quot;},{&quot;id&quot;:339,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Ney Marques e Jos\u00e9 Ant\u00f4nio Almeida acompanhando Isabel Ant\u00f4nio para a seletiva do The Voice Brasil..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Ney Marques e Jos\u00e9 Ant\u00f4nio Almeida acompanhando Isabel Ant\u00f4nio para a seletiva do The Voice Brasil..jpg_1614102215.jpg&quot;},{&quot;id&quot;:340,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Recep\u00e7\u00e3o com H\u00e9lio Magalh\u00e3es presidente do CitiBank Brasil..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Recep\u00e7\u00e3o com H\u00e9lio Magalh\u00e3es presidente do CitiBank Brasil..jpg_1614102215.jpg&quot;},{&quot;id&quot;:341,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Reg\u00eancia do maestro Jo\u00e3o Carlos Martins..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Reg\u00eancia do maestro Jo\u00e3o Carlos Martins..jpg_1614102215.jpg&quot;},{&quot;id&quot;:342,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es.jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es.jpg_1614102215.jpg&quot;},{&quot;id&quot;:343,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;4. Transporte p\u00fablico nosso parceiro para os trajetos das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Transporte p\u00fablico nosso parceiro para os trajetos das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102215.jpg&quot;},{&quot;id&quot;:344,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. A espera da primeira apresenta\u00e7\u00e3o de Isabel Ant\u00f4nio no The Voice Brasil..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. A espera da primeira apresenta\u00e7\u00e3o de Isabel Ant\u00f4nio no The Voice Brasil..jpg_1614102215.jpg&quot;},{&quot;id&quot;:345,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Apresenta\u00e7\u00e3o para o INSPIRA BB na Sala S\u00e3o Paulo..jpg_1614102215.jpg&quot;},{&quot;id&quot;:346,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Bloco de carnaval Friends4ever.jpg_1614102215.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:43:35&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Bloco de carnaval Friends4ever.jpg_1614102215.jpg&quot;},{&quot;id&quot;:347,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Comemora\u00e7\u00e3o do dia do palha\u00e7o..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Comemora\u00e7\u00e3o do dia do palha\u00e7o..jpg_1614102270.jpg&quot;},{&quot;id&quot;:348,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Dia de fazer exames com Aref..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Dia de fazer exames com Aref..jpg_1614102270.jpg&quot;},{&quot;id&quot;:349,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Final da apresenta\u00e7\u00e3o para a Campanha do Agasalho no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Final da apresenta\u00e7\u00e3o para a Campanha do Agasalho no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614102270.jpg&quot;},{&quot;id&quot;:350,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102270.jpg&quot;},{&quot;id&quot;:351,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Metr\u00f4 desde o in\u00edcio parceiro para os trajetos das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Metr\u00f4 desde o in\u00edcio parceiro para os trajetos das aulas, ensaios e grava\u00e7\u00f5es..jpg_1614102270.jpg&quot;},{&quot;id&quot;:352,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Pequena Dani no almo\u00e7o oferecido pela presid\u00eancia do Citibank..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Pequena Dani no almo\u00e7o oferecido pela presid\u00eancia do Citibank..jpg_1614102270.jpg&quot;},{&quot;id&quot;:353,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;5. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102270.jpg&quot;},{&quot;id&quot;:354,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102270.jpg&quot;},{&quot;id&quot;:355,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM ).-2.jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM ).-2.jpg_1614102270.jpg&quot;},{&quot;id&quot;:356,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102270.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:44:30&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102270.jpg&quot;},{&quot;id&quot;:357,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Bloco de carnaval Friends4ever.jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Bloco de carnaval Friends4ever.jpg_1614102333.jpg&quot;},{&quot;id&quot;:358,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102333.jpg&quot;},{&quot;id&quot;:359,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Final do almo\u00e7o oferecido pelo presidente do Citibank.jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Final do almo\u00e7o oferecido pelo presidente do Citibank.jpg_1614102333.jpg&quot;},{&quot;id&quot;:360,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102333.jpg&quot;},{&quot;id&quot;:361,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Hora do lanche antes da apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Hora do lanche antes da apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102333.jpg&quot;},{&quot;id&quot;:362,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Momento da doa\u00e7\u00e3o das camisetas que ganharam para a popula\u00e7\u00e3o vulner\u00e1vel de nosso pa\u00eds..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Momento da doa\u00e7\u00e3o das camisetas que ganharam para a popula\u00e7\u00e3o vulner\u00e1vel de nosso pa\u00eds..jpg_1614102333.jpg&quot;},{&quot;id&quot;:363,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Momento de agradecermos \u00e0 Deus!.jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Momento de agradecermos \u00e0 Deus!.jpg_1614102333.jpg&quot;},{&quot;id&quot;:364,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Na expectativa da primeira consulta de Aref..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Na expectativa da primeira consulta de Aref..jpg_1614102333.jpg&quot;},{&quot;id&quot;:365,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Ney Marques ensaiando Isabel Ant\u00f4nio para o The Voice Brasil..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Ney Marques ensaiando Isabel Ant\u00f4nio para o The Voice Brasil..jpg_1614102333.jpg&quot;},{&quot;id&quot;:366,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102333.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:45:33&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102333.jpg&quot;},{&quot;id&quot;:367,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;6.a Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102411.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6.a Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102411.jpg&quot;},{&quot;id&quot;:368,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102411.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102411.jpg&quot;},{&quot;id&quot;:369,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Bloco de carnaval Friends4ever.jpg_1614102411.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Bloco de carnaval Friends4ever.jpg_1614102411.jpg&quot;},{&quot;id&quot;:370,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Comemorando dois anos de atividades do coral junto aos integrantes..jpg_1614102411.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:51&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Comemorando dois anos de atividades do coral junto aos integrantes..jpg_1614102411.jpg&quot;},{&quot;id&quot;:371,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102411.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102411.jpg&quot;},{&quot;id&quot;:372,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Gratid\u00e3o, sempre!.jpg_1614102412.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Gratid\u00e3o, sempre!.jpg_1614102412.jpg&quot;},{&quot;id&quot;:373,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102412.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102412.jpg&quot;},{&quot;id&quot;:374,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Isabel Ant\u00f4nio e Doriana sendo patadas por um f\u00e3 em SP..jpg_1614102412.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Isabel Ant\u00f4nio e Doriana sendo patadas por um f\u00e3 em SP..jpg_1614102412.jpg&quot;},{&quot;id&quot;:375,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Momento de carinho com nosso padrinho!.jpg_1614102412.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Momento de carinho com nosso padrinho!.jpg_1614102412.jpg&quot;},{&quot;id&quot;:376,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102412.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:46:52&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102412.jpg&quot;},{&quot;id&quot;:377,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;7. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102465.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Somos Iguais em suas caminhadas para as aulas ensaios e grava\u00e7\u00f5es..jpg_1614102465.jpg&quot;},{&quot;id&quot;:378,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. A caminho da apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102465.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. A caminho da apresenta\u00e7\u00e3o na Escola Paulista de Magistratura de S\u00e3o Paulo..jpg_1614102465.jpg&quot;},{&quot;id&quot;:379,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102465.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102465.jpg&quot;},{&quot;id&quot;:380,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102465.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:45&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102465.jpg&quot;},{&quot;id&quot;:381,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Bloco de carnaval Friends4ever.jpg_1614102465.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Bloco de carnaval Friends4ever.jpg_1614102465.jpg&quot;},{&quot;id&quot;:382,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Conhecendo a CASA Chiquinha Gonzaga  Funda\u00e7\u00e3o Casa SP..jpg_1614102466.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Conhecendo a CASA Chiquinha Gonzaga  Funda\u00e7\u00e3o Casa SP..jpg_1614102466.jpg&quot;},{&quot;id&quot;:383,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102466.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102466.jpg&quot;},{&quot;id&quot;:384,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Grava\u00e7\u00e3o do Clipe do Salmo 40 do U2 com a DJ Marina Diniz..jpg_1614102466.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Grava\u00e7\u00e3o do Clipe do Salmo 40 do U2 com a DJ Marina Diniz..jpg_1614102466.jpg&quot;},{&quot;id&quot;:385,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102466.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102466.jpg&quot;},{&quot;id&quot;:386,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Isabel Ant\u00f4nio com seus professores de dan\u00e7a..jpg_1614102466.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:47:46&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Isabel Ant\u00f4nio com seus professores de dan\u00e7a..jpg_1614102466.jpg&quot;},{&quot;id&quot;:387,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Linda apresenta\u00e7\u00e3o momento de retribuir o acolhimento e a ajuda concedida pedindo doa\u00e7\u00e3o de agasalho..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Linda apresenta\u00e7\u00e3o momento de retribuir o acolhimento e a ajuda concedida pedindo doa\u00e7\u00e3o de agasalho..jpg_1614102598.jpg&quot;},{&quot;id&quot;:388,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102598.jpg&quot;},{&quot;id&quot;:389,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;8. O nosso grande abra\u00e7o no maestro no final das apresenta\u00e7\u00e3o.jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. O nosso grande abra\u00e7o no maestro no final das apresenta\u00e7\u00e3o.jpg_1614102598.jpg&quot;},{&quot;id&quot;:390,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102598.jpg&quot;},{&quot;id&quot;:391,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Bloco de carnaval Friends4ever.jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Bloco de carnaval Friends4ever.jpg_1614102598.jpg&quot;},{&quot;id&quot;:392,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Dia de reuni\u00e3o com parte da equipe..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Dia de reuni\u00e3o com parte da equipe..jpg_1614102598.jpg&quot;},{&quot;id&quot;:393,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Divers\u00e3o na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Divers\u00e3o na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102598.jpg&quot;},{&quot;id&quot;:394,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102598.jpg&quot;},{&quot;id&quot;:395,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102598.jpg&quot;},{&quot;id&quot;:396,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Momento da doa\u00e7\u00e3o das camisetas que ganharam para a popula\u00e7\u00e3o vulner\u00e1vel de nosso pa\u00eds..jpg_1614102598.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:49:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Momento da doa\u00e7\u00e3o das camisetas que ganharam para a popula\u00e7\u00e3o vulner\u00e1vel de nosso pa\u00eds..jpg_1614102598.jpg&quot;},{&quot;id&quot;:397,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102658.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Na apresenta\u00e7\u00e3o da IBM.jpg_1614102658.jpg&quot;},{&quot;id&quot;:398,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. No camarim Isabel Ant\u00f4nio e Rose Barcellos \u00e0 espera da apresenta\u00e7\u00e3o para as batalhas..jpg_1614102658.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. No camarim Isabel Ant\u00f4nio e Rose Barcellos \u00e0 espera da apresenta\u00e7\u00e3o para as batalhas..jpg_1614102658.jpg&quot;},{&quot;id&quot;:399,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;9. Um pequeno momento de conversa com o presidente do Banco do Brasil na apresenta\u00e7\u00e3o do INSPIRA BB..jpg_1614102658.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:58&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Um pequeno momento de conversa com o presidente do Banco do Brasil na apresenta\u00e7\u00e3o do INSPIRA BB..jpg_1614102658.jpg&quot;},{&quot;id&quot;:400,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102658.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Apresenta\u00e7\u00e3o na Escola Paulista da Magistratura (EPM )..jpg_1614102658.jpg&quot;},{&quot;id&quot;:401,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102659.jpg&quot;},{&quot;id&quot;:402,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Divers\u00e3o com Amon Lima na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Divers\u00e3o com Amon Lima na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102659.jpg&quot;},{&quot;id&quot;:403,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Estreia do clipe You\u2019re The Voice em Bras\u00edlia..jpg_1614102659.jpg&quot;},{&quot;id&quot;:404,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Fernanda Nobre e Isabel Ant\u00f4nio.jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Fernanda Nobre e Isabel Ant\u00f4nio.jpg_1614102659.jpg&quot;},{&quot;id&quot;:405,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102659.jpg&quot;},{&quot;id&quot;:406,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. -Isabel Ant\u00f4nio com seu t\u00e9cnico Carlinhos Brown..jpg_1614102659.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:50:59&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. -Isabel Ant\u00f4nio com seu t\u00e9cnico Carlinhos Brown..jpg_1614102659.jpg&quot;},{&quot;id&quot;:407,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;10. Na presen\u00e7a de pessoas ador\u00e1veis e respeit\u00e1veis..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Na presen\u00e7a de pessoas ador\u00e1veis e respeit\u00e1veis..jpg_1614102713.jpg&quot;},{&quot;id&quot;:408,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. A convite de alunos da ESPM fomos apresentar nosso projeto..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. A convite de alunos da ESPM fomos apresentar nosso projeto..jpg_1614102713.jpg&quot;},{&quot;id&quot;:409,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102713.jpg&quot;},{&quot;id&quot;:410,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Desde de o in\u00edcio dando voz aos integrantes..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Desde de o in\u00edcio dando voz aos integrantes..jpg_1614102713.jpg&quot;},{&quot;id&quot;:411,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Divers\u00e3o e respeito na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Divers\u00e3o e respeito na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102713.jpg&quot;},{&quot;id&quot;:412,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Estreia do clipe You\u2019re The Voice em Bras\u00edlia.jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Estreia do clipe You\u2019re The Voice em Bras\u00edlia.jpg_1614102713.jpg&quot;},{&quot;id&quot;:413,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102713.jpg&quot;},{&quot;id&quot;:414,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;11. Isabel Ant\u00f4nio conhecendo o mar pela primeira vez..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Isabel Ant\u00f4nio conhecendo o mar pela primeira vez..jpg_1614102713.jpg&quot;},{&quot;id&quot;:415,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;12. Desde o in\u00edcio dando voz aos integrantes..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Desde o in\u00edcio dando voz aos integrantes..jpg_1614102713.jpg&quot;},{&quot;id&quot;:416,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;12. Divers\u00e3o com Amon Lima na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102713.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:51:53&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Divers\u00e3o com Amon Lima na grava\u00e7\u00e3o do Clipe Salmo 40 U2..jpg_1614102713.jpg&quot;},{&quot;id&quot;:417,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;12. Grava\u00e7\u00e3o do Clipe You\u2019re The Voice no metr\u00f4 nosso maior parceiro..jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Grava\u00e7\u00e3o do Clipe You\u2019re The Voice no metr\u00f4 nosso maior parceiro..jpg_1614102798.jpg&quot;},{&quot;id&quot;:418,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;12. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102798.jpg&quot;},{&quot;id&quot;:419,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;13. Divers\u00e3o e alegria na grava\u00e7\u00e3o do Clipe Salmo 40 U2. .jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Divers\u00e3o e alegria na grava\u00e7\u00e3o do Clipe Salmo 40 U2. .jpg_1614102798.jpg&quot;},{&quot;id&quot;:420,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;13. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Grava\u00e7\u00e3o Programa Como Ser\u00e1 com J\u00falia Bandeira..jpg_1614102798.jpg&quot;},{&quot;id&quot;:421,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;13. Metr\u00f4 nosso parceiro para o trajeto das aulas e ensaios..jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Metr\u00f4 nosso parceiro para o trajeto das aulas e ensaios..jpg_1614102798.jpg&quot;},{&quot;id&quot;:422,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;14. Apresenta\u00e7\u00e3o no Teatro OPUS SP..jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:18&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Apresenta\u00e7\u00e3o no Teatro OPUS SP..jpg_1614102798.jpg&quot;},{&quot;id&quot;:423,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;14. Desde o in\u00edcio o Transporte p\u00fablico nosso parceiro para o trajeto de nossas aulas e ensaios.jpg_1614102798.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Desde o in\u00edcio o Transporte p\u00fablico nosso parceiro para o trajeto de nossas aulas e ensaios.jpg_1614102798.jpg&quot;},{&quot;id&quot;:424,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;14. Equipe participante .jpg_1614102799.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Equipe participante .jpg_1614102799.jpg&quot;},{&quot;id&quot;:425,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;15. Registro feliz com a equipe, os integrantes e seus familiares na grava\u00e7\u00e3o do Clipe Salmo 40 U2 .jpg_1614102799.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/15. Registro feliz com a equipe, os integrantes e seus familiares na grava\u00e7\u00e3o do Clipe Salmo 40 U2 .jpg_1614102799.jpg&quot;},{&quot;id&quot;:426,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;16. Ao som da Dj Marina Diniz!.jpg_1614102799.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:53:19&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/16. Ao som da Dj Marina Diniz!.jpg_1614102799.jpg&quot;},{&quot;id&quot;:427,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;17. Gravando.jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/17. Gravando.jpg_1614102874.jpg&quot;},{&quot;id&quot;:428,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;18. Muito carinho!.jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/18. Muito carinho!.jpg_1614102874.jpg&quot;},{&quot;id&quot;:429,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;19. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/19. Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102874.jpg&quot;},{&quot;id&quot;:430,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;19. S\u00f3 na divers\u00e3o..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/19. S\u00f3 na divers\u00e3o..jpg_1614102874.jpg&quot;},{&quot;id&quot;:431,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;20. Sempre de m\u00e3os dadas.jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/20. Sempre de m\u00e3os dadas.jpg_1614102874.jpg&quot;},{&quot;id&quot;:432,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;21. Uma pequena pausa..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/21. Uma pequena pausa..jpg_1614102874.jpg&quot;},{&quot;id&quot;:433,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;22. Grava\u00e7\u00e3o Clipe Salmo 40 U2..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/22. Grava\u00e7\u00e3o Clipe Salmo 40 U2..jpg_1614102874.jpg&quot;},{&quot;id&quot;:434,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;23. Fernanda Nobre sempre atenciosa com os integrantes..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/23. Fernanda Nobre sempre atenciosa com os integrantes..jpg_1614102874.jpg&quot;},{&quot;id&quot;:435,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;24. Queridas volunt\u00e1rias n\u00e3o apresenta\u00e7\u00e3o do TJSP..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/24. Queridas volunt\u00e1rias n\u00e3o apresenta\u00e7\u00e3o do TJSP..jpg_1614102874.jpg&quot;},{&quot;id&quot;:436,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;bb Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102874.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:54:34&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/bb Apresenta\u00e7\u00e3o no Tribunal de Contas do Estado de S\u00e3o Paulo..jpg_1614102874.jpg&quot;},{&quot;id&quot;:437,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (1).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (1).jpg_1614102955.jpg&quot;},{&quot;id&quot;:438,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (2).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (2).jpg_1614102955.jpg&quot;},{&quot;id&quot;:439,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (3).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (3).jpg_1614102955.jpg&quot;},{&quot;id&quot;:440,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (4).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (4).jpg_1614102955.jpg&quot;},{&quot;id&quot;:441,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (5).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (5).jpg_1614102955.jpg&quot;},{&quot;id&quot;:442,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (6).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (6).jpg_1614102955.jpg&quot;},{&quot;id&quot;:443,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (7).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (7).jpg_1614102955.jpg&quot;},{&quot;id&quot;:444,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (8).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (8).jpg_1614102955.jpg&quot;},{&quot;id&quot;:445,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;Linda entrevista com a equipe da revista Trip  (9).jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/Linda entrevista com a equipe da revista Trip  (9).jpg_1614102955.jpg&quot;},{&quot;id&quot;:446,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;xx.jpg_1614102955.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:55:55&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/xx.jpg_1614102955.jpg&quot;},{&quot;id&quot;:523,&quot;gallery_id&quot;:5,&quot;image&quot;:&quot;image00001.jpeg_1614877980.jpeg&quot;,&quot;created_at&quot;:&quot;2021-03-04 14:13:00&quot;,&quot;updated_at&quot;:&quot;2021-03-04 14:13:00&quot;,&quot;legend&quot;:&quot;Volta da vis\u00e3o de Aref&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00001.jpeg_1614877980.jpeg&quot;}]">
                        </div>
                        <p>
                            Álbum 2017
                    </p>
                    </div>
                    <div className="embed-responsive embed-responsive-16by9">
                        <div className="embed-responsive-item"
                            style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/galerias/1. A Integração durante o ensaio..jpg_1614101072.jpg')` }}
                            alt="Álbum 2016"
                            data-images="[{&quot;id&quot;:187,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. A Integra\u00e7\u00e3o durante o ensaio..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A Integra\u00e7\u00e3o durante o ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. A Integra\u00e7\u00e3o durante o ensaio..jpg_1614101072.jpg&quot;},{&quot;id&quot;:188,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. A alegria de conhecer o violino!.jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria de conhecer o violino&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. A alegria de conhecer o violino!.jpg_1614101072.jpg&quot;},{&quot;id&quot;:189,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. Dia de experimentar o figurino.jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de experimentar o figurino&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Dia de experimentar o figurino.jpg_1614101072.jpg&quot;},{&quot;id&quot;:190,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. Dia de supermercado..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de experimentar o figurino&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Dia de supermercado..jpg_1614101072.jpg&quot;},{&quot;id&quot;:191,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. Momento de carinho.jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de experimentar o figurino&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Momento de carinho.jpg_1614101072.jpg&quot;},{&quot;id&quot;:192,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. Momento de conhecer o figurino com o estilista Amir Slama..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Momento de conhecer o figurino com o estilista Amir Slama&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Momento de conhecer o figurino com o estilista Amir Slama..jpg_1614101072.jpg&quot;},{&quot;id&quot;:193,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;1. Pausa para um lanche.jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Pausa para um lanche&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Pausa para um lanche.jpg_1614101072.jpg&quot;},{&quot;id&quot;:194,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. A descoberta durante o ensaio..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A descoberta durante o ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. A descoberta durante o ensaio..jpg_1614101072.jpg&quot;},{&quot;id&quot;:195,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. Dia de circo..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A descoberta durante o ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Dia de circo..jpg_1614101072.jpg&quot;},{&quot;id&quot;:196,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. A alegria e cumplicidade no camarim..jpg_1614101072.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:24:32&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. A alegria e cumplicidade no camarim..jpg_1614101072.jpg&quot;},{&quot;id&quot;:198,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. Dia de estampar o tecido para o figurino na presen\u00e7a do estilista Amir Slama..jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de estampar o tecido para o figurino na presen\u00e7a do estilista Amir Slama&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Dia de estampar o tecido para o figurino na presen\u00e7a do estilista Amir Slama..jpg_1614101116.jpg&quot;},{&quot;id&quot;:199,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. Reuni\u00e3o com parte da equipe..jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Reuni\u00e3o com parte da equipe&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Reuni\u00e3o com parte da equipe..jpg_1614101116.jpg&quot;},{&quot;id&quot;:200,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. Dia de experimentar o figurino.jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de experimentar o figurino&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Dia de experimentar o figurino.jpg_1614101116.jpg&quot;},{&quot;id&quot;:201,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. A alegria e cumplicidade no camarim..jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim.&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. A alegria e cumplicidade no camarim..jpg_1614101116.jpg&quot;},{&quot;id&quot;:202,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. Momento de carinho.jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Momento de carinho&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Momento de carinho.jpg_1614101116.jpg&quot;},{&quot;id&quot;:203,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:16&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101116.jpg&quot;},{&quot;id&quot;:204,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;2. Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins.jpg_1614101116.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:17&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins.jpg_1614101116.jpg&quot;},{&quot;id&quot;:205,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. Aten\u00e7\u00e3o na apresenta\u00e7\u00e3o..jpg_1614101117.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:17&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Aten\u00e7\u00e3o na apresenta\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Aten\u00e7\u00e3o na apresenta\u00e7\u00e3o..jpg_1614101117.jpg&quot;},{&quot;id&quot;:206,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. Foto em apoio ao novembro azul de 2016..jpg_1614101117.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:25:17&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Foto em apoio ao novembro azul de 2016&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Foto em apoio ao novembro azul de 2016..jpg_1614101117.jpg&quot;},{&quot;id&quot;:207,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;3. Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Primeira reuni\u00e3o com o padrinho do projeto Somos Iguais, pianista e maestro Jo\u00e3o Carlos Martins..jpg_1614101174.jpg&quot;},{&quot;id&quot;:208,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;4. A alegria e cumplicidade no camarim..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. A alegria e cumplicidade no camarim..jpg_1614101174.jpg&quot;},{&quot;id&quot;:211,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;4. Foto em apoio ao novembro azul de 2016..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Foto em apoio ao novembro azul de 2016..jpg_1614101174.jpg&quot;},{&quot;id&quot;:212,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;4. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101174.jpg&quot;},{&quot;id&quot;:213,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;4. Significativos desenhos feitos para o maestro por gratid\u00e3o a oportunidade.jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Significativos desenhos feitos para o maestro por gratid\u00e3o a oportunidade&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Significativos desenhos feitos para o maestro por gratid\u00e3o a oportunidade.jpg_1614101174.jpg&quot;},{&quot;id&quot;:214,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. A alegria e cumplicidade no camarim..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. A alegria e cumplicidade no camarim..jpg_1614101174.jpg&quot;},{&quot;id&quot;:215,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;4. Uni\u00e3o durante o ensaio.jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Uni\u00e3o durante o ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Uni\u00e3o durante o ensaio.jpg_1614101174.jpg&quot;},{&quot;id&quot;:216,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. Dedica\u00e7\u00e3o para a estampa..jpg_1614101174.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:26:14&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dedica\u00e7\u00e3o para a estampa&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Dedica\u00e7\u00e3o para a estampa..jpg_1614101174.jpg&quot;},{&quot;id&quot;:217,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. Dia de ensaio com registro da presen\u00e7a dos respons\u00e1veis..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de ensaio com registro da presen\u00e7a dos respons\u00e1veis&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Dia de ensaio com registro da presen\u00e7a dos respons\u00e1veis..jpg_1614101227.jpg&quot;},{&quot;id&quot;:218,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. Divers\u00e3o e pipoca..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Divers\u00e3o e pipoca&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Divers\u00e3o e pipoca..jpg_1614101227.jpg&quot;},{&quot;id&quot;:219,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Grava\u00e7\u00e3o para o programa Fant\u00e1stico da Rede Globo..jpg_1614101227.jpg&quot;},{&quot;id&quot;:220,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. O despertar durante o ensaio..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;O despertar durante o ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. O despertar durante o ensaio..jpg_1614101227.jpg&quot;},{&quot;id&quot;:221,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;5. Pequena Dani recebendo seu primeiro buqu\u00ea de flores na presen\u00e7a do maestro Jo\u00e3o Carlos Martins..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Pequena Dani recebendo seu primeiro buqu\u00ea de flores na presen\u00e7a do maestro Jo\u00e3o Carlos Martins&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Pequena Dani recebendo seu primeiro buqu\u00ea de flores na presen\u00e7a do maestro Jo\u00e3o Carlos Martins..jpg_1614101227.jpg&quot;},{&quot;id&quot;:222,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. A alegria e cumplicidade no camarim..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. A alegria e cumplicidade no camarim..jpg_1614101227.jpg&quot;},{&quot;id&quot;:223,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Dedica\u00e7\u00e3o para a estampa.jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dedica\u00e7\u00e3o para a estampa&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Dedica\u00e7\u00e3o para a estampa.jpg_1614101227.jpg&quot;},{&quot;id&quot;:224,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Denise Chrispim Representante Acnurbrasil (ONU para Refugiados) conhecendo a pequena Dani e sua m\u00e3e..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Denise Chrispim Representante Acnurbrasil (ONU para Refugiados) conhecendo a pequena Dani e sua m\u00e3e&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Denise Chrispim Representante Acnurbrasil (ONU para Refugiados) conhecendo a pequena Dani e sua m\u00e3e..jpg_1614101227.jpg&quot;},{&quot;id&quot;:225,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Dia de ensaio com o padrinho do projeto, maestro Jo\u00e3o Carlos Martins..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Dia de ensaio com o padrinho do projeto, maestro Jo\u00e3o Carlos Martins&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Dia de ensaio com o padrinho do projeto, maestro Jo\u00e3o Carlos Martins..jpg_1614101227.jpg&quot;},{&quot;id&quot;:226,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Divers\u00e3o e pipoca..jpg_1614101227.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:27:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Divers\u00e3o e pipoca&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Divers\u00e3o e pipoca..jpg_1614101227.jpg&quot;},{&quot;id&quot;:227,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Momento de respeito..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Momento de respeito&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Momento de respeito..jpg_1614101284.jpg&quot;},{&quot;id&quot;:228,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;6. Pausa para um lanche..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Pausa para um lanche&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6. Pausa para um lanche..jpg_1614101284.jpg&quot;},{&quot;id&quot;:229,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. A alegria e cumplicidade no camarim..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;A alegria e cumplicidade no camarim&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. A alegria e cumplicidade no camarim..jpg_1614101284.jpg&quot;},{&quot;id&quot;:230,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. Amir Slama feliz com o resultado..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Amir Slama feliz com o resultado&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Amir Slama feliz com o resultado..jpg_1614101284.jpg&quot;},{&quot;id&quot;:231,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. No camarim para apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;No camarim para apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. No camarim para apresenta\u00e7\u00e3o no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo..jpg_1614101284.jpg&quot;},{&quot;id&quot;:232,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. Na espera do lanche..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Na espera do lanche&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Na espera do lanche..jpg_1614101284.jpg&quot;},{&quot;id&quot;:233,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. Nosso fiel Marcelo. .jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Nosso fiel Marcelo&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Nosso fiel Marcelo. .jpg_1614101284.jpg&quot;},{&quot;id&quot;:234,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;7. Visita do querido Sheik durante o de ensaio..jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Visita do querido Sheik durante o de ensaio&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Visita do querido Sheik durante o de ensaio..jpg_1614101284.jpg&quot;},{&quot;id&quot;:235,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;8. Momento de Divers\u00e3o .jpg_1614101284.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:28:04&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Momento de Divers\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Momento de Divers\u00e3o .jpg_1614101284.jpg&quot;},{&quot;id&quot;:237,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;8. Momento de muito carinho..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:57&quot;,&quot;legend&quot;:&quot;Momento de muito carinho&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Momento de muito carinho..jpg_1614101347.jpg&quot;},{&quot;id&quot;:238,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;8. Momento feliz. .jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Momento feliz&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Momento feliz. .jpg_1614101347.jpg&quot;},{&quot;id&quot;:239,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;8. Parte da equipe na apresenta\u00e7\u00e3o no Teatro Santander..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Parte da equipe na apresenta\u00e7\u00e3o no Teatro Santander&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Parte da equipe na apresenta\u00e7\u00e3o no Teatro Santander..jpg_1614101347.jpg&quot;},{&quot;id&quot;:240,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;9. Dia de circo com estilo e alegria.jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Dia de circo com estilo e alegria&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Dia de circo com estilo e alegria.jpg_1614101347.jpg&quot;},{&quot;id&quot;:241,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;9. Mais um trabalho conclu\u00eddo por parte da equipe. .jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Mais um trabalho conclu\u00eddo por parte da equipe&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Mais um trabalho conclu\u00eddo por parte da equipe. .jpg_1614101347.jpg&quot;},{&quot;id&quot;:242,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;9. Na exposi\u00e7\u00e3o.jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Na exposi\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Na exposi\u00e7\u00e3o.jpg_1614101347.jpg&quot;},{&quot;id&quot;:243,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;9. Ney Marques respondendo as perguntas..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:43:58&quot;,&quot;legend&quot;:&quot;Ney Marques respondendo as perguntas&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Ney Marques respondendo as perguntas..jpg_1614101347.jpg&quot;},{&quot;id&quot;:244,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;9. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101347.jpg&quot;},{&quot;id&quot;:245,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;10. Exposi\u00e7\u00e3o da trajet\u00f3ria do Coral Somos Iguais durante a apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Exposi\u00e7\u00e3o da trajet\u00f3ria do Coral Somos Iguais durante a apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101347.jpg&quot;},{&quot;id&quot;:246,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;10. A espera da apresenta\u00e7\u00e3o..jpg_1614101347.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:29:07&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. A espera da apresenta\u00e7\u00e3o..jpg_1614101347.jpg&quot;},{&quot;id&quot;:247,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;10. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101415.jpg&quot;},{&quot;id&quot;:248,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;10. O encantamento pelo piano..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. O encantamento pelo piano..jpg_1614101415.jpg&quot;},{&quot;id&quot;:249,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;11. Ensaio com gorro de papai Noel..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Ensaio com gorro de papai Noel..jpg_1614101415.jpg&quot;},{&quot;id&quot;:250,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;11. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101415.jpg&quot;},{&quot;id&quot;:251,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;11. Nossa assessora Fernanda Nobre na exposi\u00e7\u00e3o..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Nossa assessora Fernanda Nobre na exposi\u00e7\u00e3o..jpg_1614101415.jpg&quot;},{&quot;id&quot;:252,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;12. Ensaio com gorro de papai Noel..jpg_1614101415.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:15&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Ensaio com gorro de papai Noel..jpg_1614101415.jpg&quot;},{&quot;id&quot;:253,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;12. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101416.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101416.jpg&quot;},{&quot;id&quot;:254,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;12. Nossa querida Hanna com sua m\u00e3e e irm\u00e3 na exposi\u00e7\u00e3o..jpg_1614101416.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12. Nossa querida Hanna com sua m\u00e3e e irm\u00e3 na exposi\u00e7\u00e3o..jpg_1614101416.jpg&quot;},{&quot;id&quot;:255,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;13. A vista dos bastidores..jpg_1614101416.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. A vista dos bastidores..jpg_1614101416.jpg&quot;},{&quot;id&quot;:256,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;13. Ensaio com gorro de papai Noel..jpg_1614101416.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:30:16&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. Ensaio com gorro de papai Noel..jpg_1614101416.jpg&quot;},{&quot;id&quot;:257,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;13. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/13. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;},{&quot;id&quot;:258,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;14. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;},{&quot;id&quot;:259,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;14. Reuni\u00e3o com o Brigadeiro da Aeron\u00e1utica em um hospital de refer\u00eancia..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Reuni\u00e3o com o Brigadeiro da Aeron\u00e1utica em um hospital de refer\u00eancia..jpg_1614101697.jpg&quot;},{&quot;id&quot;:260,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;14. Uma pausa!.jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/14. Uma pausa!.jpg_1614101697.jpg&quot;},{&quot;id&quot;:261,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;15. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/15. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;},{&quot;id&quot;:262,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;15. No camarim.jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/15. No camarim.jpg_1614101697.jpg&quot;},{&quot;id&quot;:263,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;16. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/16. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;},{&quot;id&quot;:264,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;16. v.jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/16. v.jpg_1614101697.jpg&quot;},{&quot;id&quot;:265,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;17. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/17. No camarim para apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101697.jpg&quot;},{&quot;id&quot;:266,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;17. No camarim.jpg_1614101697.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:34:57&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/17. No camarim.jpg_1614101697.jpg&quot;},{&quot;id&quot;:267,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;18. Exposi\u00e7\u00e3o da trajet\u00f3ria do Coral Somos Iguais durante a apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/18. Exposi\u00e7\u00e3o da trajet\u00f3ria do Coral Somos Iguais durante a apresenta\u00e7\u00e3o de Natal 2016..jpg_1614101770.jpg&quot;},{&quot;id&quot;:268,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;18. O carinho com a pequena Dani! .jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/18. O carinho com a pequena Dani! .jpg_1614101770.jpg&quot;},{&quot;id&quot;:269,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;19. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo (1).jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/19. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo (1).jpg_1614101770.jpg&quot;},{&quot;id&quot;:270,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;19. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo (3).jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/19. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo (3).jpg_1614101770.jpg&quot;},{&quot;id&quot;:271,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;19. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/19. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;},{&quot;id&quot;:272,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;20. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/20. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614101770.jpg&quot;},{&quot;id&quot;:273,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;20. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/20. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;},{&quot;id&quot;:274,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;21. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/21. Apresenta\u00e7\u00e3o de Natal no Tribunal de Justi\u00e7a do Estado de S\u00e3o Paulo.jpg_1614101770.jpg&quot;},{&quot;id&quot;:275,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;21. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/21. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;},{&quot;id&quot;:276,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;22. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:36:10&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/22. Apresenta\u00e7\u00e3o de Natal.jpg_1614101770.jpg&quot;},{&quot;id&quot;:277,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;23. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/23. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;},{&quot;id&quot;:278,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;24. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/24. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;},{&quot;id&quot;:279,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;24. Os integrantes conhecendo a exposi\u00e7\u00e3o..jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/24. Os integrantes conhecendo a exposi\u00e7\u00e3o..jpg_1614101888.jpg&quot;},{&quot;id&quot;:280,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;25. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/25. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;},{&quot;id&quot;:281,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;26. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/26. Apresenta\u00e7\u00e3o de Natal.jpg_1614101888.jpg&quot;},{&quot;id&quot;:282,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;27. Momento de reconhecimento e admira\u00e7\u00e3o pelos integrantes..jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/27. Momento de reconhecimento e admira\u00e7\u00e3o pelos integrantes..jpg_1614101888.jpg&quot;},{&quot;id&quot;:283,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;28. Cumplicidade na emo\u00e7\u00e3o e na alegria..jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:08&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/28. Cumplicidade na emo\u00e7\u00e3o e na alegria..jpg_1614101888.jpg&quot;},{&quot;id&quot;:284,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;30. Confid\u00eancias.jpg_1614101888.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/30. Confid\u00eancias.jpg_1614101888.jpg&quot;},{&quot;id&quot;:285,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;29. Descanso .jpg_1614101889.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/29. Descanso .jpg_1614101889.jpg&quot;},{&quot;id&quot;:286,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;31. Registro durante a exposi\u00e7\u00e3o..jpg_1614101889.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:38:09&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/31. Registro durante a exposi\u00e7\u00e3o..jpg_1614101889.jpg&quot;},{&quot;id&quot;:287,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;42. Nosso grande abra\u00e7o de gratid\u00e3o ao nosso maestro..jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/42. Nosso grande abra\u00e7o de gratid\u00e3o ao nosso maestro..jpg_1614101954.jpg&quot;},{&quot;id&quot;:288,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00001.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00001.jpg_1614101954.jpg&quot;},{&quot;id&quot;:289,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00002.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00002.jpg_1614101954.jpg&quot;},{&quot;id&quot;:290,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00003.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00003.jpg_1614101954.jpg&quot;},{&quot;id&quot;:291,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00004.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00004.jpg_1614101954.jpg&quot;},{&quot;id&quot;:292,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00005.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00005.jpg_1614101954.jpg&quot;},{&quot;id&quot;:293,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00006.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00006.jpg_1614101954.jpg&quot;},{&quot;id&quot;:294,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00021.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00021.jpg_1614101954.jpg&quot;},{&quot;id&quot;:295,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00020.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00020.jpg_1614101954.jpg&quot;},{&quot;id&quot;:296,&quot;gallery_id&quot;:4,&quot;image&quot;:&quot;image00019.jpg_1614101954.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;updated_at&quot;:&quot;2021-02-23 14:39:14&quot;,&quot;legend&quot;:null,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/image00019.jpg_1614101954.jpg&quot;}]">
                        </div>
                        <p>
                            Álbum 2016
                    </p>
                    </div>
                    <div className="embed-responsive embed-responsive-16by9">
                        <div className="embed-responsive-item"
                            style={{ backgroundImage: `url('https://aissomosiguais.org.br/storage/galerias/1. Convite . Caminhada de Oração e Orientaçoes sobre o Câncer de Mama .jpg_1614100010.jpg')` }}
                            alt="Álbum 2015"
                            data-images="[{&quot;id&quot;:141,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;1. Convite . Caminhada de Ora\u00e7\u00e3o e Orienta\u00e7oes sobre o C\u00e2ncer de Mama .jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Convite: Caminhada de Ora\u00e7\u00e3o e Orienta\u00e7\u00f5es sobre o C\u00e2ncer de Mama&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/1. Convite . Caminhada de Ora\u00e7\u00e3o e Orienta\u00e7oes sobre o C\u00e2ncer de Mama .jpg_1614100010.jpg&quot;},{&quot;id&quot;:142,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;2. Palestrantes e organizadores da palestra Caminhada de Ora\u00e7\u00e3o..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Palestrantes e organizadores da palestra Caminhada de Ora\u00e7\u00e3o.&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2. Palestrantes e organizadores da palestra Caminhada de Ora\u00e7\u00e3o..jpg_1614100010.jpg&quot;},{&quot;id&quot;:143,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;2-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/2-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;},{&quot;id&quot;:144,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;3. Algum dos convidados presentes..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Algum dos convidados presentes&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3. Algum dos convidados presentes..jpg_1614100010.jpg&quot;},{&quot;id&quot;:145,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;3-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/3-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;},{&quot;id&quot;:146,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;4. Um caf\u00e9 ap\u00f3s a primeira a consulta..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4. Um caf\u00e9 ap\u00f3s a primeira a consulta..jpg_1614100010.jpg&quot;},{&quot;id&quot;:147,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;4-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/4-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;},{&quot;id&quot;:148,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;5. Convite de Um Ano Novo aos Refugiados..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Convite de Um Ano Novo aos Refugiados&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5. Convite de Um Ano Novo aos Refugiados..jpg_1614100010.jpg&quot;},{&quot;id&quot;:149,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;5-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/5-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;},{&quot;id&quot;:150,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;6-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:06:50&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/6-Entrega de presentes doados pelo Senac..jpg_1614100010.jpg&quot;},{&quot;id&quot;:151,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;7. Vin\u00edcius Feitosa representante da Acnurbrasil (ONU para refugiados) apoiando nossa iniciativa. .jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Vin\u00edcius Feitosa representante da Acnurbrasil (ONU para refugiados) apoiando nossa iniciativa&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/7. Vin\u00edcius Feitosa representante da Acnurbrasil (ONU para refugiados) apoiando nossa iniciativa. .jpg_1614100675.jpg&quot;},{&quot;id&quot;:152,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;8. Registro de uma parte do evento .jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Registro de uma parte do evento&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/8. Registro de uma parte do evento .jpg_1614100675.jpg&quot;},{&quot;id&quot;:153,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;9. Compra de alguns equipamentos para doa\u00e7\u00e3o. .jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Compra de alguns equipamentos para doa\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/9. Compra de alguns equipamentos para doa\u00e7\u00e3o. .jpg_1614100675.jpg&quot;},{&quot;id&quot;:154,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;10. Compra de alguns equipamentos para doa\u00e7\u00e3o. .jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Compra de alguns equipamentos para doa\u00e7\u00e3o&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/10. Compra de alguns equipamentos para doa\u00e7\u00e3o. .jpg_1614100675.jpg&quot;},{&quot;id&quot;:155,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;11. Entrega dos equipamentos doados no Clube Atl\u00e9tico Paulistano..jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega dos equipamentos doados no Clube Atl\u00e9tico Paulistano&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/11. Entrega dos equipamentos doados no Clube Atl\u00e9tico Paulistano..jpg_1614100675.jpg&quot;},{&quot;id&quot;:156,&quot;gallery_id&quot;:3,&quot;image&quot;:&quot;12-Entrega de presentes doados pelo Senac. .jpg_1614100675.jpg&quot;,&quot;created_at&quot;:&quot;2021-02-23 14:17:55&quot;,&quot;updated_at&quot;:&quot;2021-03-04 10:30:42&quot;,&quot;legend&quot;:&quot;Entrega de presentes doados pelo Senac&quot;,&quot;url&quot;:&quot;https:\/\/aissomosiguais.org.br\/storage\/galerias\/12-Entrega de presentes doados pelo Senac. .jpg_1614100675.jpg&quot;}]">
                        </div>
                        <p>
                            Álbum 2015
                    </p>
                    </div>
                </div>
                <div className="modal fade" id="modalGallery">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title titulo">
                                </h5>
                                <button type="button" className="close">
                                    <span aria-hidden="true">
                                        &times;
                                </span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <figure className="img">
                                </figure>
                                <div className="texto">
                                </div>
                                <button className="prev">
                                    <i className="fas fa-chevron-left fa-5x">
                                    </i>
                                </button>
                                <button className="next">
                                    <i className="fas fa-chevron-right fa-5x">
                                    </i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section className="contato" id="contato">
                <div className="container">
                    <div className="info-contato">
                        <h2>Afinados pela mesma causa</h2>
                        <p style={{ textAlign: 'center' }}>
                            Liberdade e dignidade n&atilde;o t&ecirc;m pre&ccedil;o, mas a contribui&ccedil;&atilde;o de
                            todos &eacute; essencial para manter esta chama acesa. Com despesas permanentes para amparo e
                            educa&ccedil;&atilde;o musical das crian&ccedil;as e jovens (que incluem material
                            did&aacute;tico, instrumentos musicais, professores, transporte para os ensaios e
                            apresenta&ccedil;&otilde;es, vestu&aacute;rio e alimenta&ccedil;&atilde;o), o Projeto
                            Humanit&aacute;rio-Coral Somos Iguais convida voc&ecirc; a se juntar a n&oacute;s. Afinal, um
                            grande espet&aacute;culo de cidadania n&atilde;o se faz apenas com a soma de talentos, mas
                            tamb&eacute;m de colaboradores sens&iacute;veis e engajados nesta miss&atilde;o.
                        </p>
                    </div>
                    <div className="row" style={{ paddingLeft: '6%' }}>
                        <div className="col-md-6">
                            <div className="contato-description">
                                <p><br />&nbsp;</p>
                            </div>
                            <ul className="dados-contato" style={{ listStyleType: 'none', paddingLeft: '8%' }}>
                                <li className="link-contact">
                                    <a href="https://www.instagram.com/coralsomosiguais">
                                        <i className="fab fa-instagram">
                                        </i>
                                    @coralsomosiguais
                                </a>
                                </li>
                                <li className="link-contact">
                                    <a href="tel:1600">
                                        <i className="fas fa-phone-volume">
                                        </i>
                                    1600
                                </a>
                                </li>
                                <li className="link-contact">
                                    <a href="mailto:contato@aissomosiguais.org.br">
                                        <i className="fas fa-envelope">
                                        </i>
                                    contato@aissomosiguais.org.br
                                </a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-6">
                            <div className="form-contato">
                                <form method="post" action="/contato">
                                    <label>
                                        Nome*:
                                </label>
                                    <br />
                                    <input name="nome" type="text" placeholder="Nome" required />
                                    <br />
                                    <label>
                                        E-mail*:
                                </label>
                                    <br />
                                    <input name="email" type="text" placeholder="E-mail" required />
                                    <br />
                                    <label>
                                        Mensagem*:
                                </label>
                                    <br />
                                    <textarea name="mensagem" cols="60" rows="5" placeholder="Deixe sua mensagem" required>
                                    </textarea>
                                    <br />
                                    <button type="submit" className="button-contato">
                                        Enviar
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer className="footer">
                <div className="logo-footer">
                    <img src="https://aissomosiguais.org.br/images/logo-blue.png" alt="Logo Coral Somos Iguais" />
                </div>
            </footer>
        </div>
        <script defer src="https://aissomosiguais.org.br/js/jquery-3.4.1.min.js">
        </script>
        <script defer src="https://aissomosiguais.org.br/js/popper.min.js">
        </script>
        <script defer src="https://aissomosiguais.org.br/js/bootstrap.min.js">
        </script>
        <script defer src="https://aissomosiguais.org.br/js/slick.js">
        </script>
    </>
    )
}
